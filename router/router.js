import Vue from "vue";
import Router from "vue-router";
import { ListUrls, RouteName } from "./routeName";

Vue.use(Router);

/**
 *
 * @param {Promise<object>} lazyComponent
 * @returns {function(): *}
 */
function lazyRouteComponentCallback(lazyComponent) {
  return () => lazyComponent.then((module) => module.default || module);
}

export function createRouter() {
  const routes = [
    {
      name: RouteName.MANAGEMENT_USER_ID,
      path: ListUrls.MANAGEMENT_USER_ID,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "management-page-user" */ "../pages/Admin/ManagementPageUser/Index.vue"
        )
      ),
    },
    {
      name: RouteName.LOGIN,
      path: ListUrls.LOGIN,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "login-page" */ "../pages/Front/Login/index.vue"
        )
      ),
    },

    {
      name: RouteName.GENCOD,
      path: ListUrls.GENCOD,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "page-gencod" */ "../pages/Front/GENCOD/index.vue"
        )
      ),
    },

    {
      name: RouteName.RESET,
      path: ListUrls.RESET,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "reset-page" */ "../pages/Front/Reset/Reset.vue"
        )
      ),
    },
    {
      name: RouteName.ORDER_CONFIRM_MEAL,
      path: ListUrls.ORDER_CONFIRM_MEAL,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "order-confirm-page" */ "../pages/Front/OrderConfirm/index.vue"
        )
      ),
    },
    {
      name: RouteName.BACKOFFICE,
      path: ListUrls.BACKOFFICE,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "back-office-page" */ "../pages/Back/BackOffice/index.vue"
        )
      ),
    },
    {
      name: RouteName.HOME,
      path: ListUrls.HOME,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "home-page" */ "../pages/Front/Home/index.vue"
        )
      ),
    },

    {
      name: RouteName.MENU_CONFIGURATOR,
      path: ListUrls.MENU_CONFIGURATOR,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "menu-configurator-page" */ "../pages/Back/MenuConfigurator/index.vue"
        )
      ),
    },

    {
      name: RouteName.RESTAURANT,
      path: ListUrls.RESTAURANT,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "restaurant-front-page" */ "../pages/Front/Restaurant/index.vue"
        )
      ),
    },

    {
      name: RouteName.RESTAURANT,
      path: ListUrls.RESTAURANT2,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "restaurant-front-page" */ "../pages/Front/Restaurant/index.vue"
        )
      ),
    },

    {
      name: RouteName.BANNER,
      path: ListUrls.BANNER,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "banner-page" */ "../pages/Back/Banner/index.vue"
        )
      ),
    },

    {
      name: RouteName.MY_ACCOUNT,
      path: ListUrls.MY_ACCOUNT,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "my-account-page" */ "../pages/Back/MyAccount/index.vue"
        )
      ),
    },
    {
      name: RouteName.ROOM_SERVICE,
      path: ListUrls.ROOM_SERVICE,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "room-service" */ "../pages/Back/RoomService/index.vue"
        )
      ),
    },
    {
      name: RouteName.ROOM_SERVICE_INTERFACE,
      path: ListUrls.ROOM_SERVICE_INTERFACE,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "room-service-interface" */ "../pages/Back/RoomService/RoomServiceInterface.vue"
        )
      ),
    },

    {
      name: RouteName.REGISTER,
      path: ListUrls.REGISTER,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "admin-register" */ "../pages/Admin/Register/index.vue"
        )
      ),
    },

    {
      name: RouteName.LIST_USERS,
      path: ListUrls.LIST_USERS,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "list-users" */ "../pages/Admin/users/index.vue"
        )
      ),
    },

    {
      name: RouteName.ORDER,
      path: ListUrls.ORDER,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "order-page" */ "../pages/Back/Order/index.vue"
        )
      ),
    },

    {
      name: RouteName.PROVIDER,
      path: ListUrls.PROVIDER,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "provider-product-shop-page" */ "../pages/Back/Provider/index.vue"
        )
      ),
    },

    {
      name: RouteName.ORDER_PRINT,
      path: ListUrls.ORDER_PRINT,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "order-print-shop-page" */ "../pages/Back/Print/index.vue"
        )
      ),
    },

    {
      name: RouteName.APPOINTMENT,
      path: ListUrls.APPOINTMENT,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "menu-configurator-page" */ "../pages/Front/Request/index.vue"
        )
      ),
    },

    // ---------- RGPD ------------------
    {
      name: RouteName.CGU,
      path: ListUrls.CGU,
      component: lazyRouteComponentCallback(
        import(/* webpackChunkName: "cgu" */ "../pages/Front/CGU/index.vue")
      ),
    },

    {
      name: RouteName.POLICY_COOKIE,
      path: ListUrls.POLICY_COOKIE,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "policy-cookie" */ "../pages/Front/CGU/policyCookie.vue"
        )
      ),
    },

    {
      name: RouteName.MENTION_LEGAL,
      path: ListUrls.MENTION_LEGAL,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "mention-legal" */ "../pages/Front/CGU/mentionsLegal.vue"
        )
      ),
    },

    {
      name: RouteName.POLICY_CONFIDENTIALITY,
      path: ListUrls.POLICY_CONFIDENTIALITY,
      component: lazyRouteComponentCallback(
        import(
          /* webpackChunkName: "policy-confidentiality" */ "../pages/Front/CGU/policyConfidentiality.vue"
        )
      ),
    },
  ];

  return new Router({
    mode: "history",
    routes,
  });
}
