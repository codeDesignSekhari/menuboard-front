export const NAV_FOOTER_CGV = "CGV";
export const NAV_FOOTER_CGU = "CGU";
export const NAV_FOOTER_POLICY_COOKIE = "Politique de cookies";
export const NAV_FOOTER_MENTION_LEGAL = "Mentions légales";
export const NAV_FOOTER_POLICY_CONFIDENTIALITY = "Politique de confidentialité";

export const HOME = "HOME";
export const RESTAURANT = "RESTAURANT";
export const RESTAURANT2 = "RESTAURANT2";

export const HOME_ROUTE_NAME = "home";
export const CONST_RESTAURANT = "Restaurant";

export const APPOINTMENT = "appointment";

export const LOGIN = "LOGIN";
const BACKOFFICE = "BACKOFFICE";
const FORGET_PASSWORD = "FORGET_PASSWORD";
const ORDER_CONFIRM_MEAL = "ORDER_CONFIRM_MEAL";
const MY_ACCOUNT = "MY_ACCOUNT";
const MENU_CONFIGURATOR = "MENU_CONFIGURATOR";
const LOGOUT = "LOGOUT";
const REGISTER = "REGISTER";
const ROOM_SERVICE = "ROOM_SERVICE";
const ROOM_SERVICE_INTERFACE = "ROOM_SERVICE_INTERFACE";
const LIST_USERS = "LIST_USERS";
const CREATE_MENU = "CREATE_MENU";
const CGU = "CGU";
const CGV = "CGV";
const MENTION_LEGAL = "MENTION_LEGAL";
const POLICY_CONFIDENTIALITY = "POLICY_CONFIDENTIALITY";
const POLICY_COOKIE = "POLICY_COOKIE";
const MANAGEMENT_USER_ID = "MANAGEMENT_USER_ID";
const RESET = "RESET";
const BANNER = "BANNER";

export const MY_ACCOUNT_FR = "Mon compte";
export const PROVIDER = "Commande fournisseur";
export const ORDER_PRINT = "Commande print";
export const MY_ORDER = "Mes commandes";
export const ROOM_SERVICE_INTERFACE_TITLE = "Prise de commande";
export const ROOM_SERVICE_TITLE = "Prise de commande main libre";
export const HOME_FR = "Accueil";
export const ORDER = "ORDER";
export const ORDER_FR = "Mes Commandes";
export const BANNER_IMAGE = "Bannière Images";

export const RouteName = {
  [HOME]: "home",
  PROVIDER: "provider",
  [LOGIN]: "login",
  ORDER_PRINT: "order-print",
  [RESET]: "reset",
  [LOGOUT]: "logout",
  [ORDER]: "ORDER",
  ORDER_FR,
  [BACKOFFICE]: "backOffice",
  [RESTAURANT]: "restaurant",
  [FORGET_PASSWORD]: "forget-password",
  [ORDER_CONFIRM_MEAL]: "order-meal-confirm",
  [MY_ACCOUNT]: "my-account",
  [MENU_CONFIGURATOR]: "menu-configurator",
  APPOINTMENT: "request-appointment",
  [REGISTER]: "register",
  [ROOM_SERVICE]: "room-service",
  [ROOM_SERVICE_INTERFACE]: "room-service-interface",
  [LIST_USERS]: "list-users",
  [CGU]: "cgu",
  [CGV]: "cgv",
  [CREATE_MENU]: "Création de menus",
  [MENTION_LEGAL]: "mentionLegal",
  [POLICY_CONFIDENTIALITY]: "policyConfidentiality",
  [POLICY_COOKIE]: "policyCookie",
  [MANAGEMENT_USER_ID]: "pageUserId",
  [BANNER]: "banner",
  GENCOD: "gencod",
};

export const ListUrls = {
  [HOME]: "/",

  [LOGIN]: "/login",
  [RESET]: "/reset/:token",

  [BACKOFFICE]: "/back/back-office",
  [RESTAURANT]: "/:city/:category/:slug",
  [RESTAURANT2]: "/:slug",
  [FORGET_PASSWORD]: "/forget-password",
  [ORDER_CONFIRM_MEAL]: "/order",
  [MY_ACCOUNT]: "/back/my-account",
  ORDER_PRINT: "/back/print",
  PROVIDER: "/back/provider",
  [MENU_CONFIGURATOR]: "/back/menu-configurator",
  [ROOM_SERVICE]: "/back/room-service",
  [ROOM_SERVICE_INTERFACE]: "/back/interface",
  [ORDER]: "/back/order",

  [REGISTER]: "/admin/register",
  [LIST_USERS]: "/admin/list-users",

  [CGU]: "/legal/cgu",
  [CGV]: "/legal/cgv",
  [MENTION_LEGAL]: "/legal/mention-legal",
  [POLICY_CONFIDENTIALITY]: "/legal/privacy-policy",
  [MANAGEMENT_USER_ID]: "/management/user/:id",
  [POLICY_COOKIE]: "/legal/cookie-policy",
  [BANNER]: "/back/banner",
  APPOINTMENT: "/request/appointment",
  GENCOD: "/gencod",
};

export const ROUTE_ADMIN_URL = {
  [RouteName.BACKOFFICE]: ListUrls.BACKOFFICE,
  [RouteName.MY_ACCOUNT]: ListUrls.MY_ACCOUNT,
  [RouteName.MENU_CONFIGURATOR]: ListUrls.MENU_CONFIGURATOR,
  [RouteName.ROOM_SERVICE]: ListUrls.ROOM_SERVICE,
  [RouteName.ROOM_SERVICE_INTERFACE]: ListUrls.ROOM_SERVICE_INTERFACE,
  [RouteName.LIST_USERS]: ListUrls.LIST_USERS,
  [RouteName.REGISTER]: ListUrls.REGISTER,
  [RouteName.ORDER]: ListUrls.ORDER,
};

export const ROUTE_ADMIN_NAME = [
  RouteName.BACKOFFICE,
  RouteName.MY_ACCOUNT,
  RouteName.MENU_CONFIGURATOR,
  RouteName.ROOM_SERVICE,
  RouteName.ROOM_SERVICE_INTERFACE,
  RouteName.LIST_USERS,
  RouteName.REGISTER,
  RouteName.ORDER,
  RouteName.ORDER_PRINT,
  RouteName.PROVIDER,
  RouteName.BANNER,
];
