/**
 https://nuxtjs.org/deployments/pm2/

 Now build your app with "npm run build".
 And serve it with "pm2 start".
 Check the status "pm2 ls".

 Your Nuxt application is now serving!
 **/
module.exports = {
  apps: [
    {
      name: "imenus",
      exec_mode: "cluster",
      instances: "max",
      script: "./node_modules/nuxt/bin/nuxt.js",
      args: "start",
    },
  ],

  deploy: {
    production: {
      user: "SSH_USERNAME",
      host: "SSH_HOSTMACHINE",
      ref: "origin/master",
      repo: "GIT_REPOSITORY",
      path: "DESTINATION_PATH",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      "pre-deploy-local": "",
      "post-deploy":
        "npm install && pm2 reload ecosystem.config.js --env production",
      "pre-setup": "",
    },
  },
};
