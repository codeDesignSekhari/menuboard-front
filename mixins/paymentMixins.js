import { generateStringRandom } from "@/components/common/utils/helpers";
import axiosClient from "@/components/common/utils/axiosClient";

export default {
  methods: {
    paymentProduct(data) {
      this.loading = true;
      this.error = false;

      const modeDelivery = this.$cookies.get("im.driving_mode");
      const payload = {
        ...data,
        ...this.cart,
        ...this.form,

        modeDelivery,
        totalPrice: this.totalPrice,
        reference: generateStringRandom,
      };

      axiosClient
        .post("payment", payload)
        .then(({ data }) => {
          if (data.status === "failed") {
            return this.$toast.error(data.message);
          }

          if (data.status === "succeeded") {
            this.$cookies.remove(`im.carts`);
            this.$store.commit("checkout/RESET_CARTS");
            this.$emit("display-succeeded-message");
            this.loading = false;
          }
        })
        .catch((e) => {
          this.error = true;
          this.errorMessage = e;
          console.error(e);
          this.loading = false;
        });
    },
  },
};
