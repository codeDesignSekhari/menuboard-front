export default {
  methods: {
    close() {
      this.$emit("update:showModal", !this.showModal);
    },
  },
};
