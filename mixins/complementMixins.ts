import { transformPrice } from "~/components/common/utils/helpers";

interface Item {
  name: string;
  price: number;
}

export default {
  methods: {
    label(item: Item): string {
      let label: string = item.name;
      let price: number = item.price;

      if (price > 0) {
        label += ` (${transformPrice(price)})`;
      }

      return label;
    },
  },
};
