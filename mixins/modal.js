export default {
  data() {
    return {
      showModal: false,
    };
  },
  methods: {
    displayModal(value = true) {
      this.showModal = value;
    },
  },
};
