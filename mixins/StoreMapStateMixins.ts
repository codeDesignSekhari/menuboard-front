import { mapState } from "vuex";

export default {
  computed: {
    ...mapState({
      restaurant: (state: any) =>
        state.fetchDefaultData?.restaurant,
    }),
  },
};
