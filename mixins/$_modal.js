export default {
  computed: {
    changeWithByDevice() {
      return this.$device.isMobileOrTablet ? 800 : 500;
    },

    checkIsMobile() {
      return !!this.$device.isMobileOrTablet;
    },
  },
};
