context("Banner cookie", () => {
  beforeEach(() => {
    Cypress.Cookies.debug(true);
    cy.visit("/");
    // cy.clearCookies("");
  });

  it("display banner cookie", () => {
    cy.popinCookie();
  });

  // it("set a browser cookie", () => {
  //   // https://on.cypress.io/setcookie
  //   cy.getCookies().should("be.empty");
  //
  //   cy.setCookie("cookieConsent", "declined");
  //
  //   cy.getCookie("cookieConsent").should("have.property", "value", "declined");
  // });
});
