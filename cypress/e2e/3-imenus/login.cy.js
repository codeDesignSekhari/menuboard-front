context("In an authenticated context", () => {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.visit("/login");
    cy.popinCookie();
    cy.fixture("storeData").then((data) => {
      this.data = data;
    });
  });

  it("form login & redirect back office", function () {
    cy.get('[data-cy="text-email"]')
      .type("zahidraza69200@gmail.com")
      .should("have.value", Cypress.env("LOGIN_TEST").email);

    cy.get('[data-cy="text-password"]')
      .type("12345678")
      .should("have.value", Cypress.env("LOGIN_TEST").password);

    cy.get('[data-cy="submit"]').click();
    cy.intercept("/back/back-office").as("getAccount");
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(8000);
    cy.url().should(
      "be.equal",
      `${Cypress.env("PUBLIC_URL_FRONT")}/back/back-office`
    );

    cy.get("h3").contains(
      `Bonjour, ${this.data.lastName.toUpperCase()} ${this.data.firstName.toUpperCase()}`
    );
    cy.get(".list-link").should("be.visible");
    cy.get(".display-navbar-menu-admin").should("be.visible");
  });
});
