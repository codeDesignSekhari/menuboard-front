context("Test page is display correctly", () => {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.visit(Cypress.env("PAGE_URL").FRONT_MENUS);
    cy.popinCookie();

    // fixtures
    cy.fixture("storeData").then((data) => (this.data = data));
    cy.fixture("categories").then((data) => (this.categories = data));
  });

  // it("Test acceptance display dom", function () {
  //   cy.getSelectorByDataCy("info-restaurant-test").should("be.visible");
  //   cy.get("h1").contains(this.data.name.toUpperCase());
  //
  //   cy.getSelectorByDataCy("link-color").contains(this.data.phoneNumber);
  //   cy.getSelectorByDataCy("opening-status-chip").should("be.visible");
  //   cy.getSelectorByDataCy("rating-number").should("be.visible");
  //   cy.getSelectorByDataCy("slider-list-items").should("be.visible");
  //   cy.getSelectorByDataCy("slider-list-items").each((row, i) => {
  //     // cy.get(row[0].innerText).should("eq", this.categories[i].name);
  //     cy.get(row).click({ force: true });
  //     expect(i).to.be.lessThan(this.categories.length + 1);
  //
  //     if (i === this.categories.length - 1) {
  //       cy.getSelectorByDataCy("btn-scroll-top").click({ force: true });
  //     }
  //   });
  // });

  it("Test acceptance product item", function () {
    // cy.getSelectorByDataCy("menu-items").length.to.eq(this.categories.length);
    cy.getSelectorByDataCy("menu-items").each((row, i) => {
      if (i !== 0) {
        console.log(cy.get(row[0].id));
      }
    });
  });
});
