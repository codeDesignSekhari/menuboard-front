// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

Cypress.Commands.add("popinCookie", () => {
  cy.get(".v-dialog").should("be.visible");
  cy.get('[data-cy="button-declined"]').click();
});

Cypress.Commands.add("getSelectorByDataCy", (selector, data) => {
  return cy.get(`[data-cy="${selector}"]`);
});
