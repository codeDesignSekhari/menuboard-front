const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require("./cypress/plugins/index.js")(on, config);
    },
    env: {
      PUBLIC_URL_FRONT: process.env.PUBLIC_URL_FRONT,
      PUBLIC_API_SERVER: process.env.SERVER_API_URL,
      LOGIN_TEST: { email: "zahidraza69200@gmail.com", password: "12345678" },
      PAGE_URL: {
        FRONT_MENUS: `/venissieux/indien-pakistanais/tandoori-house`,
      },
    },
    baseUrl: "http://localhost:3000",
  },
});
