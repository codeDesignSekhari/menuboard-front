export default function CheckoutMiddleware({ store, app }) {
  const dataCartCookie = app.$cookies.get("im.carts");
  if (dataCartCookie) {
    store.commit("checkout/RESET_CART_FROM_COOKIE", dataCartCookie);

    store.commit("checkout/SET_TOTAL_PRICE");
    store.commit("checkout/SET_TOTAL_PRICE_CHARGE");
  }
}
