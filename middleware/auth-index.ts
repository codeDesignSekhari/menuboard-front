import { RouteName } from "~/router/routeName";
import { Middleware } from '@nuxt/types';

const authIndex: Middleware = ({ store, route }) => {
  if (
    store.state.auth.loggedIn &&
    store.state.auth?.user?.store &&
    route.name !== RouteName.RESTAURANT
  ) {
    store.commit(
      "fetchDefaultData/SET_RESTAURANT",
      store.state.auth.user.store
    );
  }
}

export default authIndex;
