import { RouteName } from "@/router/routeName";
import { fetchSeoAdminUser, fetchSeoUser } from "@/config/api/api-seo";

export default async function SeoMiddleware({
  store,
  params,
  route,
  $auth,
  $config: { PUBLIC_BASE_URI },
}) {
  try {
    if (
      $auth &&
      $auth.loggedIn &&
      route.name === RouteName.MANAGEMENT_USER_ID
    ) {
      return await fetchSeoAdminUser(store, params, PUBLIC_BASE_URI);
    }

    return await fetchSeoUser(store, route, PUBLIC_BASE_URI);
  } catch (e) {
    console.error(e.response);
  }
}
