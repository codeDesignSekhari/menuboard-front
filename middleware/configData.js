export default function configDataMiddleware({ store, $axios, req, response }) {
  const user = store.getters["fetchUser/getUser"];
  const structure = store.getters["fetchDefaultData/getRestaurant"];
  $axios.defaults.headers.common["x-custom-auth"] = JSON.stringify({
    id: user?.id,
    roles: user?.roles,
    type: structure ? "BACK" : "ADMIN",
    uid: structure?.uid,
    name: structure?.name,
    category: structure?.category?.name,
  });
}
