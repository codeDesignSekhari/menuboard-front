import { fetchRestaurantData } from "@/config/api/api-restaurant";

export default async function fetcherDataMiddleware({
  params,
  store,
  route,
  error,
  redirect,
}) {
  // example : https://127.0.0.1:8000/v1/api/restaurants/Lyon-3/japonnais/sushi-shop
  try {
    return await fetchRestaurantData(store, params, route, error, redirect);
  } catch (e) {
    console.error(e);
  }
}
