import axios from "axios";

export const getRoutes = () => {
  return new Promise(async (resolve, reject) => {
    const events = await fetchSlug();
    console.log(events);
    const routes = [];

    for (const event of events) {
      const route = {
        url: `/${event.name}`,
      };
      routes.push(route);
    }

    resolve(routes);
  });
};

const fetchSlug = () => {
  return axios
    .get(`${process.env.SERVER_API_URL}/slugs`)
    .then((response) => response["hydra:member"]);
};
