export default function generateStringRandom() {
  const length = 6;
  const chars = "AZERTYUIOPQSDFGHJKLMWXCVBN1234567890";

  let value = "";
  for (let i = 0; i < length; ++i) {
    value += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return value ?? "";
}
