import https from "https";
import { BACK } from "@/components/common/utils/const-roles";
import { setClient } from "@/components/common/utils/axiosClient";

export default function ({ app: { $axios, $cookies, store } }) {
  $axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false });

  $axios.onRequest((config) => {
    const url = config.url.replace(config.baseURL, "");
    const splitUrl = url.split("/");
    const hasBackInUrl = splitUrl.includes(BACK);

    if (hasBackInUrl && $cookies.get("auth._token.local")) {
      config.headers.Authorization = `${$cookies.get("auth._token.local")}`;
    }

    return config;
  });

  $axios.onResponse((response) => {
    return response;
  });

  // $axios.onError();

  setClient($axios);
}
