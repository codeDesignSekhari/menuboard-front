import Vue from "vue";
import vuePhoneNumberInput from "vue-phone-number-input";
import "vue-phone-number-input/dist/vue-phone-number-input.css";
Vue.use(vuePhoneNumberInput);
