import Vue from "vue";
import { StripePlugin } from "@vue-stripe/vue-stripe";

export default function ({ store }) {
  if (window.Stripe && store.state.fetchDefaultData) {
    Vue.use(StripePlugin, {
      pk: "pk_test_iGX1WWt4cmBmvG86JCZTknAy",
      // stripeAccount: "acct_1JGPa2PrkkhzSJKu",
      locale: "fr_FR",
    });
  }
}
