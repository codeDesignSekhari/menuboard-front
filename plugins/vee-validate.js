import { extend } from "vee-validate";
import {
  required,
  email,
  max,
  digits,
  integer,
  regex,
  min,
  length,
  confirmed,
} from "vee-validate/dist/rules";

extend("email", {
  ...email,
  message: "L'email n'est pas valide",
});

extend("integer", {
  ...integer,
  message: "Le champs doit contenir uniquement des chiffre.",
});

extend("min", {
  ...min,
  message: "minimum {length} caractères requis.",
});

extend("confirmed", {
  ...confirmed,
  message: "Le mot de passe n'est pas identique",
});

extend("max", {
  ...max,
  message: "La description ne peut pas dépasser {length} caractères",
});

extend("regex", {
  ...regex,
  message: "{_field_} {_value_} does not match {regex}",
});

extend("required", {
  ...required,
  message: "La valeur doit être renseignée.",
});

extend("length", {
  ...length,
  message: "La description ne peut pas dépasser {length} caractères",
});

extend("digits", {
  ...digits,
  message: "{_field_} needs to be {length} digits. ({_value_})",
});
