export default function ({ $gtm, route, store, $cookies }) {
  if ($cookies.get("cookieConsent") === "full") {
    $gtm.init(process.env.GTM);

    // const fetchDefaultData = store.state.fetchDefaultData;
    // $gtm.push({
    //   route: route.path,
    //   country: fetchDefaultData?.restaurant?.country ?? "",
    //   restaurantName: fetchDefaultData?.restaurant?.name ?? "",
    // });
  }
}
