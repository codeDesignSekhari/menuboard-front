import { ListUrls, RouteName } from "@/router/routeName";

export default function authPlugin({
  $auth,
  store,
  route,
  $config: { PUBLIC_URL_FRONT },
}) {
  if ($auth?.loggedIn) {
    store.commit("fetchDefaultData/SET_RESTAURANT", $auth.user.store);
    store.commit("fetchUser/SET_TO_USER", $auth.user);

    if (route.name === RouteName.LOGIN) {
      window.location.href = `${PUBLIC_URL_FRONT}${ListUrls.BACKOFFICE}`;
    }
  }
}
