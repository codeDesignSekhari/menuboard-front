import getInvoicesByUser from "@/components/BackOffice/Invoice/services/api-invoices";

export const state = () => ({
  invoices: [],
});

export const mutations = {
  SET_INVOICES(state, payload) {
    state.invoices = payload;
  },
};

// getter
export const getters = {
  getInvoices(state) {
    return state.invoices;
  },
};

export const actions = {
  async GET_INVOICES_USER({ commit }, value) {
    try {
      const { data } = await getInvoicesByUser(value.stripeKey);
      commit("SET_INVOICES", data);
      return data;
    } catch (e) {
      console.error(e);
    }
  },
};
