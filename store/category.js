import fetchCategories, {
  postRank,
  putCategoriesMenu,
  removeMenuTitleAndChildren,
} from "../components/BackOffice/MenuConfigurator/utils/api-menu-title";
import axiosClient from "@/components/common/utils/axiosClient";

export const state = () => ({
  categories: [],
  message: "",
  isLoading: false,
});

// setter
export const mutations = {
  ADD_MENU_TITLE(state, payload) {
    state.categories = payload;
  },

  UPDATE_FIELD_NAME_CATEGORY(state, payload) {
    state.categories[payload.index].name = payload.value;
  },

  REMOVE_CATEGORY(state, index) {
    state.categories.splice(index, 1);
  },

  SET_MENU_TITLES(state, payload) {
    state.categories = payload;
  },
  SET_IS_LOADING(state, payload) {
    state.isLoading = payload;
  },
};

// getter
export const getters = {
  getIsLoading: (state) => {
    return state.isLoading;
  },

  getCategoryWithChildren: (rootState) => {
    return rootState.menuConfigurators.filter((item) => item.children > 0);
  },
};

export const actions = {
  async REMOVE_CATEGORY_AND_FETCH({ commit, dispatch }, index) {
    commit("REMOVE_CATEGORY", index);
    await dispatch("MenuConfigurator/FETCH_MENUS_CONFIGURATOR", {
      root: true,
    });
  },
  async FETCH_CATEGORIES({ commit, rootState }) {
    try {
      const { data } = await fetchCategories(rootState.auth?.user?.store?.id);
      commit("ADD_MENU_TITLE", data);
    } catch (e) {
      console.error(e.response.data);
    }
  },
  async POST_CATEGORY_MENU({ commit, dispatch }, { name, storeId }) {
    try {
      const { data } = await axiosClient.post(`menu_titles`, {
        name,
        createAt: new Date(),
        store: `/v1/api/stores/${storeId}`,
        ranked: 1,
      });

      if (data.type !== "hydra:Error") {
        await dispatch("FETCH_CATEGORIES");
        return data;
      }
    } catch (e) {
      console.error(e);
    }
  },
  async PUT_CATEGORY_MENU({ dispatch, commit, app }, values) {
    try {
      const { data } = await putCategoriesMenu(values);

      await dispatch("FETCH_CATEGORIES");

      await dispatch("MenuConfigurator/FETCH_MENUS_CONFIGURATOR", {
        root: true,
      });

      this.$toast.success(data.message);
    } catch (e) {
      console.error(e);
    }
  },
  async REMOVE_CATEGORY_MENU_AND_CHILDREN({ dispatch, commit }, values) {
    try {
      const { data } = await removeMenuTitleAndChildren(values.id);
      await dispatch("FETCH_CATEGORIES");
      await dispatch("MenuConfigurator/FETCH_MENUS_CONFIGURATOR", {
        root: true,
      });

      this.$toast.success(data.message);

      return data.data;
    } catch (e) {
      console.error(e);
    }
  },
  async POST_RANK({ dispatch, commit }, value) {
    try {
      const res = await postRank(value);

      if (res && res.status === 200) {
        await dispatch("MenuConfigurator/FETCH_MENUS_CONFIGURATOR", {
          root: true,
        });
      }
    } catch (e) {
      console.error(e);
    }
  },
};
