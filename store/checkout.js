import postBackOrder, {
  patchBackOrder,
} from "@/components/common/utils/api-order";
import axiosClient from "@/components/common/utils/axiosClient";
// import generateStringRandom from "@/helpers/reference-helper";

const fields = {
  email: null,
  // address: null,
  phone: null,
};

export const state = () => ({
  fields,
  carts: null,
  status: 0,
  orders: [],
  orderWaiting: null,
  hasPaymentStatus: false,
  charge: 0,
  totalPriceCharge: 0,
  deliveryPrice: 0,
  totalPrice: 0,
  quantity: 1,
  productItem: undefined,
  promoCode: undefined,
  enablePurchase: true,
});

// setter
export const mutations = {
  UPDATE_FIELD(state, { property, value }) {
    state.fields[property] = value;
  },
  RESET_CART_FROM_COOKIE(state, payload) {
    state.carts = payload;
  },
  ADD_TO_CART(state, payload) {
    if (!state.carts) {
      state.carts = payload;
    } else {
      const identicalOrderIndex = state.carts.orders.findIndex(
        (item) => item.id === payload.id
      );

      if (identicalOrderIndex !== -1) {
        state.carts.orders[identicalOrderIndex].quantity += payload.quantity;
      } else {
        state.carts.orders.push(payload);
      }
    }
  },
  RESET_CARTS(state) {
    state.carts = null;
  },
  ADD_OPEN_CARD_BASKET(state, payload) {
    state.openCardBasket = payload;
  },
  DELETE_ITEM_CART(state, idx) {
    state.carts.orders.splice(idx, 1);
  },
  ADD_PAYMENT_STATUS(state, payload) {
    state.status = payload;
  },
  UPDATE_CART(state, payload) {
    const itemCart = state.carts.orders.find((c) => c.index === payload.index);
    itemCart.price = itemCart.price + payload.unit_price;
  },
  SUBTRACT_CART(state, payload) {
    const itemCart = state.carts.orders.find((c) => c.index === payload.index);
    if (payload.price > payload.unit_price && payload.unit_price !== 0) {
      itemCart.price = itemCart.price - payload.unit_price;
    }
  },
  ADD_QUANTITY(state) {
    state.quantity += 1;
  },
  SUBTRACT_QUANTITY(state) {
    if (state.quantity === 1) {
      console.log("Zero or Negative quantity not allowed");
    } else {
      state.quantity -= 1;
    }
  },
  INFOS_PAYMENT_STATUS(state, payload) {
    state.hasPaymentStatus = payload;
  },
  SET_TOTAL_PRICE(state) {
    state.totalPrice = state.carts?.orders?.reduce(
      (sum, { price, quantity }) => sum + price * quantity,
      0
    );
  },
  SET_TOTAL_PRICE_CHARGE(state) {
    state.totalPriceCharge = state.totalPrice
      ? state.totalPrice + state.charge
      : 0;
  },
  SET_QUANTITY_PRODUCT(state, payload) {
    state.quantity = payload;
  },
  RESET_QUANTITY(state) {
    state.quantity = 1;
  },
  SET_ORDERS(state, payload) {
    state.orders = payload;
  },
  UPDATE_CART_QUANTITY(state, { idx, quantity }) {
    state.carts.orders[idx].quantity = quantity;
  },

  SUB_CART_QUANTITY(state, { idx, quantity }) {
    state.carts.orders[idx].quantity = quantity;
  },
};

// getter
export const getters = {
  // example
  // getPiggyById: (state) => (id) => {
  //   return state.breeds.find(breed => breed.id == id)
  // }
  getCart(state) {
    return state.carts;
  },
  getPaymentStatus(state) {
    return state.status;
  },
  hasPaymentStatus(state) {
    return state.hasPaymentStatus;
  },
  getTotalPrice(state) {
    return state.totalPrice;
  },
  getDeliveryPrice(state) {
    return state.deliveryPrice;
  },
  getCharge(state) {
    return state.charge;
  },
  mergedEquivalentItem(state) {
    return state.carts.orders.find((item) => item.name);
  },
};

export const actions = {
  updateQuantityCart({ commit, state }, payload) {
    commit("UPDATE_CART_QUANTITY", payload);
    commit("SET_TOTAL_PRICE");
    commit("SET_TOTAL_PRICE_CHARGE");
  },
  subQuantityCart({ commit, state }, { idx, quantity }) {
    if (state.carts.orders[idx].quantity === 0) {
      // state.quantity = 0;
      commit("DELETE_ITEM_CART", idx);
      commit("SET_TOTAL_PRICE");
      commit("SET_TOTAL_PRICE_CHARGE");

      return state.carts.orders[idx].quantity;
    } else {
      commit("SUB_CART_QUANTITY", { idx, quantity });
      commit("SET_TOTAL_PRICE");
      commit("SET_TOTAL_PRICE_CHARGE");
    }
  },
  addTotalPrice({ commit, state }, payload) {
    commit("SET_TOTAL_PRICE", payload);
  },
  fetchOrderByStore({ commit, rootState }, payload) {
    axiosClient
      .get(`stores/${rootState.auth?.user?.store?.uid}/orders`, payload)
      .then(({ data }) => commit("SET_ORDERS", data));
  },
  postOrderCart({ commit, state, dispatch, rootState }, payload) {
    return axiosClient.post(`carts/store/${payload.storeUid}/orders`, payload);
  },
  async postOrder({ commit, state, dispatch }, payload) {
    return await axiosClient
      .post(`${this.$config.PUBLIC_BASE_URI}/orders`, payload)
      .then(({ data }) => {
        dispatch("postOrderLine", payload.orders);
      });
  },
  async postOrderLine({ commit, state }, payload) {
    return await axiosClient
      .post(`${this.$config.PUBLIC_BASE_URI}/orderLines`, payload)
      .then(({ data }) => {});
  },
  addCartItem({ commit, state }, data) {
    try {
      commit("ADD_TO_CART", data);
      commit("SET_TOTAL_PRICE");
    } catch (e) {
      console.error(e);
    }
  },
  openBasket({ commit }, payload) {
    commit("ADD_OPEN_CARD_BASKET", payload);
  },
  async POST_ORDER_WAITING({ commit, router }, payload) {
    try {
      const res = await postBackOrder(payload.data);
      if (res) {
        router.push({ name: "order-meal-confirm" });
      }
    } catch (e) {
      console.error(e);
    }
  },
  async PATCH_ORDER_SUCCESS({ commit }, payload) {
    try {
      const res = await patchBackOrder(payload.data);
      if (res) {
        commit("RESET_CARTS");
        commit("INFOS_PAYMENT_STATUS", true);
        commit("ADD_PAYMENT_STATUS", res.status);
        return res.data;
      }
    } catch (e) {
      commit("ADD_PAYMENT_STATUS", 400);
      commit("INFOS_PAYMENT_STATUS", false);
      console.error(e);
    }
  },
  deleteOrder({ commit, state }, index) {
    commit("DELETE_ITEM_CART", index);

    this.$cookies.set("im.carts", state.carts);
    commit("SET_TOTAL_PRICE");
    commit("SET_TOTAL_PRICE_CHARGE");
  },
  updateItemCart({ commit, state }, payload) {
    commit("UPDATE_CART", payload);
  },
  subtractItemCart({ commit, state }, payload) {
    commit("SUBTRACT_CART", payload);
  },
  decrementQuantityProduct({ commit }) {
    commit("SUBTRACT_QUANTITY");
  },
  addQuantityProduct({ commit }) {
    commit("ADD_QUANTITY");
  },
  cleanQuantityProduct({ commit }) {
    commit("RESET_QUANTITY");
  },
  displayCart({ commit, state }, payload) {
    commit("SHOW_CART", payload);
  },
};
