import axiosClient from "@/components/common/utils/axiosClient";
import { RESTAURANT, RouteName } from "@/router/routeName";

export const state = () => ({
  banners: [],
  banner: {
    name: "",
    startTime: null,
    endTime: null,
    device: false,
    picture: null,
  },
});

// setter
export const mutations = {
  SET_BANNERS(state, payload) {
    state.banners = payload;
  },
  SET_BANNER(state, payload) {
    state.banner = payload;
  },
  SET_BANNER_END_DATE(state, payload) {
    state.banner.endTime = payload;
  },
  SET_BANNER_START_DATE(state, payload) {
    state.banner.startTime = payload;
  },
  SET_BANNER_NAME(state, payload) {
    state.banner.name = payload;
  },
  SET_BANNER_DEVICE(state, payload) {
    state.banner.device = payload;
  },

  SET_BANNER_PICTURE(state, payload) {
    state.banner.picture = payload;
  },

  ADD_BANNER(state, payload) {
    state.banners.push(payload);
  },
  REMOVE_BANNER(state, id) {
    const bannerIndex = state.banners.findIndex((b) => b.id === id);

    state.banners.splice(bannerIndex, 1);
  },
};

// getter
export const getters = {};

export const actions = {
  fetchBanners({ commit, rootState }) {
    let url = `banners?store.uid=${
      rootState.fetchDefaultData.restaurant?.uid ??
      rootState.auth?.user?.store?.uid
    }`;

    if (this.$router.currentRoute.name === RouteName[RESTAURANT]) {
      const now = new Date().toISOString();
      const format = now.split("T")[0];
      url += `&startTime[before]=${format}&endTime[strictly_after]=${format}&active=1&device=${
        rootState.fetchDefaultData.device.isMobile ? "mobile" : "desktop"
      }`;
    }

    axiosClient
      .get(`${url}`)
      .then(({ data }) => commit("SET_BANNERS", data["hydra:member"]));
  },

  fetchBannersFront({ commit, rootState }) {
    let url = `banner/slider?store.uid=${
      rootState.fetchDefaultData.restaurant?.uid ??
      rootState.auth?.user?.store?.uid
    }`;

    const now = new Date().toISOString();
    const format = now.split("T")[0];
    url += `&startTime=${format}&active=1&device=${
      rootState.fetchDefaultData.device.isMobile ? "mobile" : "desktop"
    }`;

    axiosClient.get(`${url}`).then(({ data }) => commit("SET_BANNERS", data));
  },

  CREATE({ commit }, payload) {
    try {
      return axiosClient.post(`banners/upload`, payload).then(({ data }) => {
        this.$toast.success("La banniére a bien été créé");
        commit("ADD_BANNER", data);
        return data;
      });
    } catch (e) {
      this.$toast.error(
        "Une erreur est survenue lors de la création de la banniére publicitaire"
      );
      console.error(e);
    }
  },
  UPDATE({ commit }, { id, data }) {
    try {
      return axiosClient.put(`banners/${id}`, data).then(({ data }) => {
        this.$toast.success("La banniére a été correctement modifiée");
        return data;
      });
    } catch (e) {
      this.$toast.error(
        "Une erreur est survenue lors de la création de la banniére publicitaire"
      );
      console.error(e);
    }
  },
  DELETE({ commit }, { id }) {
    try {
      return axiosClient.delete(`banners/${id}`).then(({ data }) => {
        commit("REMOVE_BANNER", id);
        this.$toast.success("La banniére a été correctement supprimée");

        return data;
      });
    } catch (e) {
      this.$toast.error("Une erreur est survenue");
      console.error(e);
    }
  },
};
