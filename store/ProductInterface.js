export const state = () => ({
  products: [],
});

export const mutations = {
  SET_PRODUCT_INTERFACE(state, payload) {
    state.products.push(payload);
  },
  SET_PRODUCT_COUNT(state, { idx, product }) {
    state.products[idx] = product;
  },

  REMOVE(state, i) {
    state.products.splice(i, 1);
  },
};

// getter
export const getters = {
  countProduct: (state) => (value) => {
    for (let i = 0; i < state.products.length; i++) {
      if (state.products[i].id === value.id) {
        return state.products[i].count;
      }
    }

    return 0;
  },
};

export const actions = {};
