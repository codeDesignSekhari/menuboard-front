import axiosClient from "@/components/common/utils/axiosClient";

export const state = () => ({
  file: undefined,
  picture: undefined,
  context: undefined,
});

// setter
export const mutations = {
  UPLOAD_FILE(state, payload) {
    state.file = payload;
  },
  SET_CONTEXT(state, payload) {
    state.context = payload;
  },
};

// getter
export const getters = {};

export const actions = {
  async UPLOAD_FILE_CLOUDINARY({ state, rootState }) {
    if (state.file) {
      return await this.$cloudinary.upload(state.file, {
        folder: rootState.auth.user.store.folderNamePicture ?? "Tandoori",
        uploadPreset: "imenus_upload",
      });
    }
  },
  async UPDATE_UPLOAD_FILE({ commit }, payload) {
    return await axiosClient.post(`/back/media-manager`, payload);
  },
};
