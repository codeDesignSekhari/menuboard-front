import moment from "moment";
import {
  deleteSchedule,
  fetchDayWeekAndTypePayment,
  postIsShopStore,
} from "@/components/BackOffice/MyAccount/service/api-account";

import {
  CLOSE,
  FRIDAY,
  MONDAY,
  OPEN,
  SATURDAY,
  SUNDAY,
  THURSDAY,
  TUESDAY,
  WEDNESDAY,
} from "@/components/BackOffice/MyAccount/service/const-time-open";
import { isEmpty } from "@/components/common/utils/helpers";
import axiosClient from "~/components/common/utils/axiosClient";

export const state = () => ({
  device: {},
  restaurants: [],
  restaurant: undefined,
  categories: [],
  scheduleFront: {
    title: "Nos Horaires",
    icon: "mdi-clock-time-eight-outline",
    data: [
      {
        [MONDAY]: {
          open: "",
          close: "",
        },
        [TUESDAY]: {
          open: "",
          close: "",
        },
        [WEDNESDAY]: {
          open: "",
          close: "",
        },
        [THURSDAY]: {
          open: "",
          close: "",
        },
        [FRIDAY]: {
          open: "",
          close: "",
        },
        [SUNDAY]: {
          open: "",
          close: "",
        },
        [SATURDAY]: {
          open: "",
          close: "",
        },
      },
    ],
  },
  shopConf: {
    schedule: {
      data: [
        {
          [OPEN]: "",
          [CLOSE]: "",
          id: 0,
          schedule: [
            {
              model: MONDAY,
              checked: false,
            },
            {
              model: TUESDAY,
              checked: false,
            },
            {
              model: WEDNESDAY,
              checked: false,
            },
            {
              model: THURSDAY,
              checked: false,
            },
            {
              model: FRIDAY,
              checked: false,
            },
            {
              model: SATURDAY,
              checked: false,
            },
            {
              model: SUNDAY,
              checked: false,
            },
          ],
        },
      ],
      title: "",
      icon: "",
    },
    typePayment: {
      cash: false,
      creditCard: false,
      ticket: false,
      icon: "mdi-credit-card-outline",
      title: "Moyen de paiement",
    },
    detailCosts: {
      deliveryCost: 0,
      minimumOrder: 0,
      icon: "mdi-moped-outline",
      title: "Minimum de commande",
    },
  },
  hasScrollY: 0,
});

// setter
export const mutations = {
  SET_STORE_BOOKING(state, payload) {
    state.restaurant.booking = payload;
  },
  SET_STORE_CLOSED(state, payload) {
    state.restaurant.disabledShopStore = payload;
  },
  SET_STORE_ECOMMERCE(state, payload) {
    state.restaurant.isShopStore = payload;
  },
  SET_DEVICE(state, payload) {
    state.device = payload;
  },
  SET_SCROLL_Y(state, payload) {
    state.hasScrollY = payload;
  },
  SET_RESTAURANT(state, payload) {
    state.restaurant = payload;
  },
  SET_CATEGORIES(state, payload) {
    state.categories = payload;
  },
  SET_ORDER_MENU(state, payload) {
    state.orderMenus = payload;
  },
  ADD_SHOP_CONF_DAY_WEEK(state) {
    state.shopConf.schedule.data.push({
      [OPEN]: "",
      [CLOSE]: "",
      id: 0,
      schedule: [
        {
          model: MONDAY,
          checked: false,
        },
        {
          model: TUESDAY,
          checked: false,
        },
        {
          model: WEDNESDAY,
          checked: false,
        },
        {
          model: THURSDAY,
          checked: false,
        },
        {
          model: FRIDAY,
          checked: false,
        },
        {
          model: SATURDAY,
          checked: false,
        },
        {
          model: SUNDAY,
          checked: false,
        },
      ],
    });
  },
  SET_SHOP_CONF_TYPE_PAYMENT(state, payload) {
    state.shopConf.typePayment = payload;
  },
  SET_SHOP_CONF_SCHEDULE(state, payload) {
    state.shopConf.schedule = payload;
  },
  SET_SHOP_CONF_SCHEDULE_FRONT(state, payload) {
    state.scheduleFront = payload;
  },
  UPDATE_FORM_DAY_WEEK_OPEN_AT(state, payload) {
    state.shopConf.schedule.data[payload.index].open = payload.value;
  },
  UPDATE_FORM_DAY_WEEK_CLOSE_AT(state, payload) {
    state.shopConf.schedule.data[payload.index].close = payload.value;
  },
  UPDATE_FORM_DAY_WEEK_SCHEDULE(state, payload) {
    state.shopConf.schedule.data[payload.indexParent].schedule[
      payload.index
    ].checked = payload.value;
  },

  UPDATE_CHECKBOX_TYPE_CASH(state, payload) {
    state.shopConf.typePayment.cash = payload.value;
  },
  UPDATE_CHECKBOX_TYPE_CREDIT_CARD(state, payload) {
    state.shopConf.typePayment.creditCard = payload.value;
  },
  UPDATE_CHECKBOX_TYPE_TICKET(state, payload) {
    state.shopConf.typePayment.ticket = payload.value;
  },
  UPDATE_FORM_DELIVERY_COST(state, payload) {
    state.shopConf.detailCosts.deliveryCost = parseInt(payload.value);
  },
  UPDATE_FORM_MINIMUM_ORDER(state, payload) {
    state.shopConf.detailCosts.minimumOrder = parseInt(payload.value);
  },
  SET_FORM_DETAIL_COST_LIST(state, payload) {
    state.shopConf.detailCosts = payload;
  },

  UPDATE_FORM_PHONE_NUMBER(state, payload) {
    state.restaurant.phoneNumber = payload;
  },

  SET_SWITCH_IS_SHOW_CASE(state, payload) {
    state.restaurant.isShopStore = payload;
  },
  SET_SWITCH_DISABLED_STORE(state, payload) {
    state.restaurant.disabledShopStore = payload;
  },

  REMOVE_SCHEDULE(state, payload) {
    state.shopConf.schedule.data.splice(payload, 1);
  },

  SET_KEY_STRIPE(state, payload) {
    state.restaurant.configShop.stripeKeyPrivate = payload;
  },
};

// getter
export const getters = {
  getRestaurant(state) {
    return state.restaurant;
  },
  getConfiguration(state) {
    return state.conf;
  },
  categories(state) {
    return state.categories;
  },

  openFromNow: (state) => (value) => {
    return moment(value).fromNow();
  },

  isOpeningStore(state) {
    let isOpening = false;

    const weekday = new Date().toLocaleString("en-us", { weekday: "long" });
    // const weekDays = moment.weekdays();

    // moment("2023-01-21T07:14:00-08:00").fromNow()

    const currentHourTime = moment(new Date()).format("LTS");

    if (state.scheduleFront?.data && state.scheduleFront.data.length) {
      state.scheduleFront.data.forEach((item, index) => {
        const weekData = Object.entries(item).find((value) => {
          return (
            value[1][0] &&
            !isEmpty(value[1][0].close) &&
            !isEmpty(value[1][0].open) &&
            value[0] === weekday.toLowerCase()
          );
        });

        if (weekData) {
          const open = moment(weekData[1][0]?.open, "HH:mm A").format(
            "HH:mm:ss"
          );

          const close = moment(weekData[1][0]?.close, "HH:mm A").format(
            "HH:mm:ss"
          );

          if (open && close) {
            isOpening = currentHourTime > open && currentHourTime < close;
          }

          if (isOpening) {
            return isOpening;
          }
        }
      });
    }
  },

  getScheduleFront: (state) => state.scheduleFront.data,
};

// async treatment data
export const actions = {
  async fetchDataDayWeekAndTypePayment({ commit }, { id, role }) {
    await fetchDayWeekAndTypePayment(id, role)
      .then(({ data }) => {
        const results = data;

        if (role) {
          commit("SET_SHOP_CONF_SCHEDULE", results.schedule);
        } else {
          commit("SET_SHOP_CONF_SCHEDULE_FRONT", results.schedule);
        }

        commit("SET_SHOP_CONF_TYPE_PAYMENT", results.typePayment);
        commit("SET_FORM_DETAIL_COST_LIST", results.detailCosts);
      })
      .catch((e) => {
        console.error(e.response.data);
      });
  },

  async removeSchedule({ commit }, { id, index }) {
    if (id === 0) {
      commit("REMOVE_SCHEDULE", index);
    } else {
      await deleteSchedule(id)
        .then(() => {
          commit("REMOVE_SCHEDULE", index);
        })
        .catch((e) => {
          console.error(e.response.data);
        });
    }
  },

  async postIsShopStoreSwitch({ commit }, payload) {
    await postIsShopStore(payload).then(({ data }) => {
      commit("SET_SWITCH_IS_SHOW_CASE", payload.value);
    });
  },

  async postIsClosedOnlineSwitch({ commit, state }, payload) {
    await axiosClient
      .put(`stores/${state.restaurant.uid}`, {
        disabledShopStore: payload,
      })
      .then(({ data }) => {
        commit("SET_SWITCH_DISABLED_STORE", data.disabledShopStore);
      });
    /** await postIsClosedOnline(payload).then(({ data }) => {
      commit("SET_SWITCH_DISABLED_STORE", payload.value);
      this.$toast.success(data.message);
    });**/
  },
};
