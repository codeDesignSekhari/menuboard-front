import removeMenuConfigurator, {
  postMenuConfigurator,
  updateMenuConfiguratorById,
} from "~/components/BackOffice/MenuConfigurator/utils/api-menus-configurator";
import removeMediaManager from "~/components/common/utils/api-uploader";
import setStatusConfigurator, {
  setStatusInStock,
} from "~/components/TheHeader/Services/api-status-configurator";
import { childMenuConfigurator } from "@/components/common/utils/const-menu-configurator";
import { ERROR_API } from "@/components/common/messages/messageError";
import { handleMessage } from "@/components/common/HandleMessage";
import axiosClient from "@/components/common/utils/axiosClient";

export const state = () => ({
  menuConfigurators: [],
  menuConfiguratorChildren: [],
  childMenuConfigurator,
  error: false,
  isLoading: false,
  file: null,
  mediaManger: null,
  itemsMenus: [],
  activePanel: null,
  complementGroups: [],
});

// getter
export const getters = {
  getChildrenMenuConfigByCategoryId: (state) => (id) => {
    if (id === undefined) {
      id = state.menuConfigurators[0].id;
    }

    const menuConfig = state.menuConfigurators.find((item) => item.id === id);

    if (menuConfig.children.length) {
      return menuConfig.children.map((item) => {
        return {
          id: item.id,
          name: item.name,
          mediaManager: item.mediaManager,
          price: item.price,
          priceMenu: item.priceMenu,
          subTitle: item.subTitle,
          inStock: item.inStock,
          allergen: item.allergen,
          children: item.complementGroups,
        };
      });
    }

    return [];
  },
  getFile(state) {
    return state.file;
  },
  getIndexMenuConfiguratorById: (state) => (parentId, childId) => {
    const menuConfiguratorIdx = state.menuConfigurators.findIndex(
      (item) => item.id === parentId
    );

    const menuConfiguratorChildIdx = state.menuConfigurators[
      menuConfiguratorIdx
    ].children.findIndex((item) => item.id === childId);

    return {
      menuConfiguratorIdx,
      menuConfiguratorChildIdx,
    };
  },
  getComplementGroups(state) {
    return state.complementGroups;
  },

  getMenuConfiguratorById: (state) => (id) => {
    return state.menuConfigurators.find((item) => item.id === id);
  },

  getMenuConfiguratorChildrenById: (state) => (parentId, childId) => {
    const menuConfigurator = state.menuConfigurators.find(
      (item) => item.id === parentId
    );
    return menuConfigurator.children.find((item) => item.id === childId);
  },

  getMenus(state) {
    return state.menuConfigurators;
  },
};

// setter
export const mutations = {
  SET_IS_LOADING(state, payload) {
    state.isLoading = payload;
  },
  SET_TO_MENU_CONFIGURATOR(state, payload) {
    state.menuConfigurators = payload;
  },
  SET_MENU_CONFIGURATOR_CHILDREN(state, payload) {
    state.menuConfiguratorChildren = payload;
  },
  ADD_MENU_CONFIGURATOR(state, payload) {
    if (payload) {
      const currentItem = state.menuConfigurators.find(
        (item) => item.id === payload.menuTitle.id
      );

      const element = payload;

      if (currentItem && currentItem.children.length > 0) {
        const currentIndexItem = currentItem.children.findIndex(
          (item) => item.id === element.id
        );
        if (element.status === "clone") {
          currentItem.children.push(element);
        } else {
          currentItem.children[currentIndexItem] = element;
        }
      } else {
        state.menuConfigurators.push(element);
      }
    } else {
      throw new Error(`payload is not defined`);
    }
  },
  DELETE_MENU_CONFIGURATOR(state, item) {
    const menuTitleIndex = state.menuConfigurators.findIndex(
      (mt) => mt.name === item.menuTitle.name
    );

    const childIndex = state.menuConfigurators[
      menuTitleIndex
    ].children.findIndex((im) => im.id === item.id);

    state.menuConfigurators[menuTitleIndex].children.splice(childIndex, 1);
  },
  SET_TO_DATA_FILE(state, payload) {
    state.file = payload;
  },
  RESET_FILE_MEDIA_MANAGER(state) {
    state.file = null;
  },

  UPDATE_FORM_ITEM_MENU(state, payload) {
    if (payload.childIndex !== undefined) {
      state.complementGroups[payload.index].complements[payload.childIndex][
        payload.key
      ] = payload.value;
    } else {
      state.complementGroups[payload.index][payload.key] = payload.value;
    }
  },
  REMOVE_ITEM_MENU(state, payload) {
    state.complementGroups.splice(payload.index, payload.value);
  },
  REMOVE_ELEMENT_ITEM_MENU(state, { idxGroup, idxElement }) {
    state.complementGroups[idxGroup].complements.splice(idxElement, 1);
  },
  ADD_ITEM_MENU(state, payload) {
    state.complementGroups.push(payload);
  },
  ADD_VALUES_FORM_ITEM_MENU(state, { index }) {
    state.complementGroups[index].complements.push({
      id: 0,
      title: "",
      price: 0,
    });
  },
  SET_COMPLEMENT_GROUP(state, payload) {
    state.complementGroups = payload;
  },
  SET_ONE_CHILD_MENU_CONFIGURATOR(state, payload) {
    state.childMenuConfigurator = payload;
  },
  CLEAN_COMPLEMENT_GROUP(state) {
    state.complementGroups = [];
  },

  UPDATE_FORM_NAME(state, payload) {
    state.childMenuConfigurator.name = payload;
  },
  UPDATE_FORM_SUB_TITLE(state, payload) {
    state.childMenuConfigurator.subTitle = payload;
  },
  UPDATE_FORM_PRICE(state, payload) {
    state.childMenuConfigurator.price = payload;
  },
  UPDATE_FORM_PRICE_MENU(state, payload) {
    state.childMenuConfigurator.priceMenu = payload;
  },
  UPDATE_FORM_DESCRIPTION(state, payload) {
    state.childMenuConfigurator.description = payload;
  },
  UPDATE_FORM_ALLERGEN(state, payload) {
    state.childMenuConfigurator.allergen = payload;
  },
  UPDATE_FORM_CATEGORY(state, payload) {
    state.childMenuConfigurator.menuTitle = payload;
  },
  SET_SWITCH_PREVIEW(state, payload) {
    state.menuConfigurators[payload.idx.menuConfiguratorIdx].children[
      payload.idx.menuConfiguratorChildIdx
    ].preview = payload.value;
  },
  SET_SWITCH_IN_STOCK(state, { idx, value }) {
    state.menuConfigurators[idx.menuConfiguratorIdx].children[
      idx.menuConfiguratorChildIdx
    ].inStock = value;
  },
  RESET_MENU_CONFIGURATOR(state) {
    state.childMenuConfigurator = {
      id: undefined,
      "@id": undefined,
      mediaManager: {
        id: undefined,
        name: undefined,
        fileName: undefined,
        size: undefined,
      },
      priceMenu: undefined,
      name: undefined,
      price: undefined,
      allergen: undefined,
      subTitle: undefined,
      description: undefined,
      supplement: undefined,
      supplementPrice: undefined,
      menuTitle: {
        id: undefined,
        name: undefined,
      },
      complementGroups: [],
    };
  },
  SET_ERROR(state, payload) {
    state.error = payload;
  },
  SET_ACTIVE_PANEL(state, payload) {
    state.activePanel = payload;
  },

  UPDATE_FORM_SUPPLEMENT(state, payload) {
    state.childMenuConfigurator.supplement = payload;
  },
  UPDATE_FORM_SUPPLEMENT_PRICE(state, payload) {
    state.childMenuConfigurator.supplementPrice = payload;
  },
};

// call action dispatch
export const actions = {
  ADD_DATA_FILE({ commit }, data) {
    commit("SET_TO_DATA_FILE", data);
  },
  async REMOVE_MEDIA_MANAGER({ commit }, value) {
    try {
      const { data } = await removeMediaManager(value.id);
      commit("SET_MESSAGE", data.data);
    } catch (e) {
      console.error(e);
    }
  },
  async REMOVE_MENU_CONFIGURATOR({ commit, dispatch }, item) {
    try {
      const { data } = await removeMenuConfigurator(item);

      if (data) {
        commit("DELETE_MENU_CONFIGURATOR", item.child);
        await dispatch("FETCH_MENUS_CONFIGURATOR");
        this.$toast.success(data.message);
      }
    } catch (e) {
      console.error(e);
    }
  },
  async SET_STATUS_MENU_CONFIGURATOR({ commit }, values) {
    try {
      return await setStatusConfigurator(values);
    } catch (e) {
      console.error(e);
    }
  },
  async SET_STATUS_IN_STOCK_MENU_CONFIGURATOR({ commit }, values) {
    try {
      return await setStatusInStock(values);
    } catch (e) {
      console.error(e);
    }
  },
  FETCH_MENUS_CONFIGURATOR({ commit, rootState }) {
    try {
      commit("SET_IS_LOADING", true);
      return axiosClient
        .get(`menu_configurators/store/${rootState.auth.user.store.id}`)
        .then(({ data }) => {
          commit("SET_TO_MENU_CONFIGURATOR", data);
          commit("SET_IS_LOADING", false);

          return data;
        });
    } catch (e) {
      console.error(e);
    }
  },
  async CREATE_MENU_CONFIGURATOR({ dispatch, commit, rootState }, values) {
    try {
      commit("SET_IS_LOADING", true);

      const { data } = await postMenuConfigurator(values);

      if (data.data) {
        commit("ADD_MENU_CONFIGURATOR", data.data);

        await dispatch("FETCH_MENUS_CONFIGURATOR");
        commit("SET_IS_LOADING", false);
        commit("CLEAN_COMPLEMENT_GROUP");
        commit("RESET_FILE_MEDIA_MANAGER");
        commit("RESET_MENU_CONFIGURATOR");

        this.$toast.success(data.message);
      }
    } catch (e) {
      dispatch("API_ERROR", {
        e,
      });
      commit("SET_IS_LOADING", false);
    }
  },
  UPDATE_MENU_CONFIGURATOR({ dispatch, commit, rootState }, values) {
    try {
      commit("SET_IS_LOADING", true);
      updateMenuConfiguratorById(values).then(async ({ data }) => {
        commit("ADD_MENU_CONFIGURATOR", data.data);
        await dispatch("FETCH_MENUS_CONFIGURATOR");
        this.$toast.success(data.message);
        commit("SET_IS_LOADING", false);
      });
    } catch (e) {
      dispatch("API_ERROR", {
        e,
      });

      commit("SET_IS_LOADING", false);
    }
  },
  async FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR({ commit, state }) {
    try {
      return await axiosClient
        .get(
          `complement_groups?menuConfigurator.id=${state.childMenuConfigurator.id}`
        )
        .then(({ data }) => {
          commit("SET_COMPLEMENT_GROUP", data["hydra:member"]);
        });
    } catch (e) {
      console.error(e);
    }
  },
  async POST_COMPLEMENT_GROUP({ dispatch, commit, state }, payload) {
    try {
      await axiosClient
        .post(
          `/menu_configurators/${state.childMenuConfigurator.id}/complements/add`,
          {
            isMultiple: payload.isMultiple,
            name: payload.name,
            max: parseInt(payload.max),
            min: parseInt(payload.min),
            complements: payload.complements,
            nbField: parseInt(payload.nbField),
          }
        )
        .then(() => {
          this.$toast.success("La formule a bien été ajoutée");
          dispatch("FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR");
        });
      /** const { data } = await axiosClient.post("complement_groups", {
        isMultiple: payload.isMultiple,
        name: payload.name,
        max: parseInt(payload.max),
        min: parseInt(payload.min),
        nbField: parseInt(payload.nbField),
        complement: payload.complements,
        menuConfigurator: `/v1/api/menu_configurators/${state.childMenuConfigurator.id}`,
      });

      if (data) {
        payload.complements.forEach((value, complementIdx) => {
          dispatch("POST_COMPLEMENT", {
            name: value.name,
            price: parseFloat(value.price),
            complementGroup: data["@id"],
          });

          if (
            payload?.complements &&
            payload.complements.length === complementIdx + 1
          ) {
            this.$toast.success("La formule a bien été ajoutée");
            dispatch("FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR");
          }
        });
      }
      **/
    } catch (e) {
      this.$toast.error(e);
      console.error(e);
    }
  },
  async PUT_COMPLEMENT_GROUP({ dispatch, commit, state }, payload) {
    try {
      await axiosClient
        .put(
          `/menu_configurators/${state.childMenuConfigurator.id}/complements/add`,
          {
            isMultiple: payload.isMultiple,
            name: payload.name,
            max: parseInt(payload.max),
            min: parseInt(payload.min),
            nbField: parseInt(payload.nbField),
            complementGroupId: payload.id,
            complements: payload.complements,
          }
        )
        .then(() => {
          this.$toast.success("La formule a bien été modifiée");
          dispatch("FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR");
        });
    } catch (e) {
      this.$toast.error(e);
      console.error(e);
    }
  },
  async POST_COMPLEMENT({ dispatch, commit }, value) {
    try {
      await axiosClient.post("complements", value);
    } catch (e) {
      this.$toast.error(e);
      console.error(e);
    }
  },
  async PUT_COMPLEMENT({ dispatch, commit }, value) {
    try {
      const { data } = await axiosClient.put(`complements/${value.id}`, value);

      if (data) {
        await dispatch("FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR");
      }
    } catch (e) {
      this.$toast.error(e);
      console.error(e);
    }
  },
  CLEAR_FORM_ITEM_MENUS({ commit }) {
    commit("CLEAN_COMPLEMENT_GROUP");
  },
  ADD_AND_UPDATE_FIELD_FROM_SELECTOR(
    { dispatch, commit, state, getters },
    data
  ) {
    const complementGroups = getters.getComplementGroups;
    const loop = Math.abs(
      complementGroups[data.index].complements.length - data.value
    );

    if (data.value < complementGroups[data.index].complements.length) {
      data.lengthItems = complementGroups[data.index].complements.length;

      // remove complement && remove element from array
      complementGroups[data.index].complements.forEach(
        async (element, idxElement) => {
          if (idxElement > data.value - 1) {
            if (element.id !== 0) {
              await dispatch("REMOVE_ELEMENT_ITEM", element.id);
            }

            commit("REMOVE_ELEMENT_ITEM_MENU", {
              idxGroup: data.index,
              idxElement,
            });
          }
        }
      );

      axiosClient.put(`complement_groups/${data.id}`, {
        nbField: data.value,
      });

      data.value = complementGroups[data.index].complements.length;
    } else if (complementGroups[data.index].complements.length + loop <= 10) {
      for (let i = 0; i < loop; i++) {
        commit("ADD_VALUES_FORM_ITEM_MENU", data);
      }
    }

    commit("UPDATE_FORM_ITEM_MENU", data);
  },

  REMOVE_ELEMENT_ITEM({ dispatch, commit }, id) {
    axiosClient.delete(`complements/${id}`);
  },

  REMOVE_COMPLEMENT_GROUP({ commit, dispatch }, values) {
    if (values.item.id) {
      axiosClient.delete(`complement_groups/${values.item.id}`).then(() => {
        commit("REMOVE_ITEM_MENU", values);
        dispatch("FETCH_COMPLEMENT_GROUPS_FILTER_MENU_CONFIGURATOR");
      });
    } else {
      commit("REMOVE_ITEM_MENU", values);
    }
  },

  UPDATE_CHILD_PREVIEW({ commit, getters }, payload) {
    const idx = getters.getIndexMenuConfiguratorById(
      payload.parentId,
      payload.id
    );
    commit("SET_SWITCH_PREVIEW", { idx, value: payload.value });
  },

  UPDATE_CHILD_IN_STOCK({ commit, getters }, payload) {
    const idx = getters.getIndexMenuConfiguratorById(
      payload.parentId,
      payload.id
    );
    commit("SET_SWITCH_IN_STOCK", { idx, value: payload.value });
  },
  async FETCH_MENU_CONFIGURATOR_CHILDREN_BY_CATEGORY_ID(
    { state, getters, dispatch, commit },
    id
  ) {
    commit("SET_IS_LOADING", true);
    await dispatch("FETCH_MENUS_CONFIGURATOR");

    if (state.menuConfigurators.length) {
      const children = getters.getChildrenMenuConfigByCategoryId(id);
      commit("SET_MENU_CONFIGURATOR_CHILDREN", children);

      commit("SET_IS_LOADING", false);
    }
  },
  API_ERROR({ commit, getters }, { e, message }) {
    commit("SET_ERROR", true);
    console.error(e);
    handleMessage(this.$toast, message || ERROR_API, "error");
  },
};
