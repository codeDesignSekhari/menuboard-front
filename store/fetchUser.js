import axiosClient from "@/components/common/utils/axiosClient";

export const state = () => ({
  currentUser: undefined,
  users: [],
  user: undefined,
  categories: [],
});

// setter
export const mutations = {
  SET_TO_USER(state, payload) {
    state.currentUser = payload;
  },
  SET_TO_CATEGORIES(state, payload) {
    state.categories = payload;
  },
  SET_USERS(state, payload) {
    state.users = payload;
  },
  SET_USER_ID(state, payload) {
    state.user = payload;
  },

  SET_CONFIG_SHOP_COLOR(state, payload) {
    state.currentUser.store.configShop.colorBase = payload;
  },
};

// getter
export const getters = {
  getUser(state) {
    return state.currentUser;
  },
  categories(state) {
    return state.categories;
  },
  getUsers(state) {
    return state.users.map((value) => value.user);
  },
};

export const actions = {
  async fetchUserAuth({ commit }) {
    const { data, error } = await axiosClient.get(
      `${this.$config.PUBLIC_BASE_URI}/back/user_check`
    );

    if (data) {
      commit("fetchDefaultData/SET_RESTAURANT", data.user.store, {
        root: true,
      });
      commit("fetchUser/SET_TO_USER", data.user, {
        root: true,
      });
    }

    if (error) {
      console.error(error);
    }
  },
  async fetchListUsers({ commit }) {
    const { data } = await axiosClient
      .get(`${this.$config.PUBLIC_BASE_URI}/admin/users`)
      .catch((e) => {
        console.error(e);
      });

    commit("SET_USERS", data.data);
  },
  async fetchUserById({ commit }, payload) {
    const { data } = await axiosClient
      .get(`${this.$config.PUBLIC_BASE_URI}/admin/user/${payload.id}`)
      .catch((e) => {
        console.error(e);
      });

    commit("SET_USER_ID", data.data);
  },
  setUserById({ commit, state }, id) {
    const user = state.users.find((value) => value.id === id);
    commit("SET_USER_ID", user);
  },
};
