import axiosClient from "@/components/common/utils/axiosClient";

export const state = () => ({
  seo: undefined,
  inputValues: {
    id: 0,
    data: [
      { title: "" },
      { metaDescription: "" },
      { metaKeyWords: "" },
      { ldJson: "" },
      { metaRobots: "" },
    ],
  },
});

// setter
export const mutations = {
  SET_SEO(state, payload) {
    state.inputValues = payload;
  },
  SET_SEO_FRONT(state, payload) {
    state.seo = payload;
  },

  UPDATE_FORM_SEO(state, payload) {
    state.inputValues.data[payload.index][payload.key] = payload.value;
  },
};

// getter
export const getters = {
  getSeo(state) {
    return state.inputValues;
  },
  getSeoFront(state) {
    return state.seo;
  },
};

export const actions = {
  async FETCH_SEO({ commit }, payload) {
    const { data } = await axiosClient.get(
      `${process.env.baseURL}/admin/seo/user/${payload}`
    );
    commit("SET_SEO", data.data.seo);
  },
  async createSeo({ commit, dispatch }, payload) {
    payload.value.userId = payload.id;

    await axiosClient.post(`/admin/seo/create`, payload.value);
    await dispatch("FETCH_SEO", payload.id);
  },
  async updateSeo({ commit }, payload) {
    const { data } = await axiosClient.patch(`/admin/seo`, payload);

    commit("SET_SEO", data.data);
  },
};
