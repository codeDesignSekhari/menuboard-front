import {
  ListUrls,
  NAV_FOOTER_CGU,
  NAV_FOOTER_MENTION_LEGAL,
  NAV_FOOTER_POLICY_CONFIDENTIALITY,
  NAV_FOOTER_POLICY_COOKIE,
} from "~/router/routeName";

const footerNavs = [
  {
    name: NAV_FOOTER_CGU,
    link: ListUrls.CGU,
  },
  {
    name: NAV_FOOTER_POLICY_COOKIE,
    link: ListUrls.POLICY_COOKIE,
  },
  {
    name: NAV_FOOTER_MENTION_LEGAL,
    link: ListUrls.MENTION_LEGAL,
  },
  {
    name: NAV_FOOTER_POLICY_CONFIDENTIALITY,
    link: ListUrls.POLICY_CONFIDENTIALITY,
  },
];

export default footerNavs;
