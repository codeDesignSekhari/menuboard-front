import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_REMOVE = "/back/menu-configurator";
const URL_API_GET_MENUS_CONFIGURATORS = "/back/user";
const URL_API_POST_MENU_CONFIGURATOR = "/back/menu-configurator";
const URL_API_POST_RANK_MENU_CONFIGURATOR = "/back/menu-configurator/rank";
const URL_API_POST_BEST_SALE_MENU_CONFIGURATOR =
  "/back/menu-configurator/best-sale";
const URL_API_POST_ITEM_MENU = "/back/item-menu/create";
const URL_API_DELETE_ELEMENT_ITEM_MENU = "/back/element-item-menu/";
const URL_API_DELETE_ITEM_MENU = "/back/item-menu/";

export default function removeMenuConfigurator(data) {
  return axiosClient.delete(
    `${process.env.SERVER_API_URL}${URL_API_REMOVE}/${data.id}/delete`
  );
}

export function getMenuConfiguratorsByUser(id) {
  return axiosClient.get(
    `${process.env.SERVER_API_URL}${URL_API_GET_MENUS_CONFIGURATORS}/${id}/menus-configurators`
  );
}

export function postMenuConfigurator(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_MENU_CONFIGURATOR}`,
    data
  );
}

export function updateMenuConfiguratorById(data) {
  return axiosClient.patch(
    `${process.env.SERVER_API_URL}${URL_API_POST_MENU_CONFIGURATOR}`,
    data
  );
}

export function postRankMenuConfigurator(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_RANK_MENU_CONFIGURATOR}`,
    data
  );
}

export function postBestSaleMenuConfigurator(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_BEST_SALE_MENU_CONFIGURATOR}`,
    data
  );
}

export function postItemMenu(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_ITEM_MENU}`,
    data
  );
}

export function deleteElementItemMenu(id) {
  return axiosClient.delete(
    `${process.env.SERVER_API_URL}${URL_API_DELETE_ELEMENT_ITEM_MENU}${id}/delete`
  );
}

export function deleteItemMenu(id) {
  return axiosClient.delete(
    `${process.env.SERVER_API_URL}${URL_API_DELETE_ITEM_MENU}${id}/delete`
  );
}
