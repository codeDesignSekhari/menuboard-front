import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_POST_CATEGORY_MENU = "/back/menu-title/create";
const URL_API_PUT_CATEGORY_MENU = "/back/menu-title";
const URL_API_POST_RANK = "/back/menu-title/rank";
const URL_API_REMOVE_MENU_TITLE_MENU_CONFIGURATOR = "/menu-title/";

export default function fetchCategories(id) {
  return axiosClient.get(
    `stores/${id}/menu_titles?groups[]=countMenuConfigurator`
  );
}

export function addMenuTitles(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_CATEGORY_MENU}`,
    data
  );
}

export function putCategoriesMenu(data) {
  return axiosClient.patch(
    `${process.env.SERVER_API_URL}${URL_API_PUT_CATEGORY_MENU}`,
    data
  );
}

export function postRank(data) {
  return axiosClient.post(`${process.env.SERVER_API_URL}${URL_API_POST_RANK}`, data);
}

export function removeMenuTitleAndChildren(id) {
  return axiosClient.delete(
    `${process.env.SERVER_API_URL}${URL_API_REMOVE_MENU_TITLE_MENU_CONFIGURATOR}${id}/delete`
  );
}
