import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_POST_API_KEY_STRIPE = "/back/stripe";
const URL_API_POST_SHOP_CONFIG_ACCOUNT = "/back/config-shop/config";
const URL_API_GET_SHOP_CONFIG_SHOP_DAY_WEEK_PAYMENT_TYPE = "/config-shop/";

const URL_API_POST_UPDATE_PHONE_NUMBER = "/back/store/";
const URL_API_POST_SWITCH_IS_SHOW_CASE = "/back/store/is-show-case";
const URL_API_POST_CHANGE_PASSWORD = "/admin/forgot-password";
const URL_API_POST_SWITCH_IS_CLOSED_ONLINE =
  "/back/store/is-disabled-shop-config";

export default function postApiKey(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_API_KEY_STRIPE}`,
    data
  );
}

export function postConfigShopAccount(params) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_SHOP_CONFIG_ACCOUNT}`,
    params
  );
}

export function fetchDayWeekAndTypePayment(id, role = null) {
  const urlWithout = `${process.env.SERVER_API_URL}${URL_API_GET_SHOP_CONFIG_SHOP_DAY_WEEK_PAYMENT_TYPE}${id}/day-week-payment`;
  const url = role ? `${urlWithout}?role=${role}` : urlWithout;
  return axiosClient.get(url);
}

export function postPhoneNumber(uid, value) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_UPDATE_PHONE_NUMBER}change-phone-number`,
    {
      uid,
      phoneNumber: value,
    }
  );
}

export function postIsShopStore(value) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_SWITCH_IS_SHOW_CASE}`,
    value
  );
}

export function postIsClosedOnline(value) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_SWITCH_IS_CLOSED_ONLINE}`,
    value
  );
}

export function postForgetPassword(id, value) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_CHANGE_PASSWORD}`,
    {
      data: {
        id,
        value,
      },
    }
  );
}

export function deleteSchedule(id) {
  const URL_API_DELETE_SCHEDULE = `/back/schedule/${id}/remove`;

  return axiosClient.delete(`${process.env.SERVER_API_URL}${URL_API_DELETE_SCHEDULE}`);
}
