export const CLOSE_OO = "00:00";

export const MONDAY = "monday";
export const TUESDAY = "tuesday";
export const WEDNESDAY = "wednesday";
export const THURSDAY = "thursday";
export const FRIDAY = "friday";
export const SATURDAY = "saturday";
export const SUNDAY = "sunday";

export const Lundi = "Lundi";
export const Mardi = "Mardi";
export const Mercredi = "Mercredi";
export const Jeudi = "Jeudi";
export const Vendredi = "Vendredi";
export const Samedi = "Samedi";
export const Dimanche = "Dimanche";

export const OPEN = "open";
export const CLOSE = "close";

export const OUVERT = "OUVERT";
export const FERMER = "Fermé";

export const WEEK_MAP = [
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
];

export const SCHEDULE_MAP = {
  [MONDAY]: Lundi,
  [TUESDAY]: Mardi,
  [WEDNESDAY]: Mercredi,
  [THURSDAY]: Jeudi,
  [FRIDAY]: Vendredi,
  [SATURDAY]: Samedi,
  [SUNDAY]: Dimanche,
};

export const openingStoreHours = {
  [OPEN]: OUVERT,
  [CLOSE]: FERMER,
};
