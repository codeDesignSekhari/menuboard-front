import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_GET_INVOICES_USER = "/back/invoices/";

export default function getInvoicesByUser(customerStripe) {
  return axiosClient.get(
    `${process.env.SERVER_API_URL}${URL_API_GET_INVOICES_USER}${customerStripe}`
  );
}
