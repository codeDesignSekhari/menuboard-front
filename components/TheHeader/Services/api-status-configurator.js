import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_POST_STATUS_CONFIGURATOR = "/back/menu-configurator/online";
const URL_API_GET_STATUS_IN_STOCK = "/back/menu-configurator/instock";

export default function setStatusConfigurator(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_STATUS_CONFIGURATOR}`,
    data
  );
}

export function setStatusInStock(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_GET_STATUS_IN_STOCK}`,
    data
  );
}
