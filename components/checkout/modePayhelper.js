export const PAYPAL_COOKIE = "PAYPAL";
export const CREDIT_CARD_COOKIE = "CREDIT_CARD";
export const CRYPTO_COOKIE = "CRYPTO";

export const CREDIT_CARD = 0;
export const PAYPAL = 1;
export const CRYPTO = 2;

export const MODE_PAY_MAP = {
  [PAYPAL_COOKIE]: PAYPAL,
  [CREDIT_CARD_COOKIE]: CREDIT_CARD,
  [CRYPTO_COOKIE]: CRYPTO,
};

export const MODE_PAY_COOKIE_MAP = {
  [PAYPAL]: PAYPAL_COOKIE,
  [CREDIT_CARD]: CREDIT_CARD_COOKIE,
  [CRYPTO]: CRYPTO_COOKIE,
};
