// error api
export const ERROR_API =
  "Une erreur serveur s'est produite, Veuillez réessayer dans quelques instants.";
export const ERROR_FILE_NOT_FOUND =
  "L'image est un introuvable, ajouter une image";

export const ERROR_SERVER_UPLOAD_FR =
  "Le serveur a répondu avec le code 0, Veuillez réessayer dans quelques instants.";

// message stripe
export const MESSAGE_STRIPE_KEY_NOT_EXIST =
  "Veuillez fournir les informations Stripe dans la section mon compte.";
export const MESSAGE_TITLE_STRIPE_KEY_NOT_EXIST =
  "Nous avons suspendu les fonctions de paiement sur votre compte";

export const MESSAGE_UPDATE_SUCCESS = "Mise a jour effectuée";
// login
export const ERROR_LOGIN = "Email ou mot de passe incorrect";
export const SESSION_EXPIRE_LOGOUT =
  "La session d'authentification a expiré. Vous allez être reconnecté dans quelques instants";

export const MESSAGES_ERROR_MAP = Object.freeze({
  [ERROR_API]: ERROR_API,
  [MESSAGE_STRIPE_KEY_NOT_EXIST]: MESSAGE_STRIPE_KEY_NOT_EXIST,
  [ERROR_LOGIN]: ERROR_LOGIN,
});
