import { URL_API_POST_MEDIA_MANAGER } from "@/components/common/utils/api-uploader";

export function dropZoneConfig(cookies) {
  return {
    options: {
      url: `${process.env.SERVER_API_URL}${URL_API_POST_MEDIA_MANAGER}`,
      acceptedFiles: "image/*, application/pdf",
      addRemoveLinks: true,
      uploadMultiple: false,
      maxFiles: 1,
      method: "post",
      duplicateCheck: true,
      paramName: "upload",
      headers: {
        Authorization: `${this.$cookies.get("auth._token.local")}`,
        enctype: "multipart/form-data",
        "Cache-Control": null,
        "X-Requested-With": null,
      },
    },
  };
}
