import {
  CONTEXT_IMAGE_COVER,
  CONTEXT_IMAGE_PICTURE,
  CONTEXT_IMAGE_PROFILE,
} from "@/components/BackOffice/MenuConfigurator/Items/type-image";

const getAssetId = (context, restaurant = {}, itemMenu = []) => {
  switch (context) {
    case CONTEXT_IMAGE_COVER:
      return restaurant?.picture?.cover?.assetId;

    case CONTEXT_IMAGE_PROFILE:
      return restaurant?.picture?.profile?.assetId;

    case CONTEXT_IMAGE_PICTURE:
      if (Object.keys(itemMenu).length) {
        return itemMenu?.picture?.assetId;
      }
  }

  return null;
}

export default getAssetId;
