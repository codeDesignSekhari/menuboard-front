const ALL = "Tous"; // adds trad
const ALL_LOWERCASE = "tous"; // trad

export default {
  ALL_LOWERCASE,
  ALL
}
