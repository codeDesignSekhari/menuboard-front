export function handleMessage(toast, message, context = "success") {
  switch (context) {
    case "error": {
      return toast.global.error(message);
    }

    case "success": {
      return toast.global.success(message);
    }

    case "warning": {
      return toast.global.warning(message);
    }
  }
}
