import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_DELETE_MEDIA_MANAGER = "/back/media-manager";
export const URL_API_POST_MEDIA_MANAGER = "/back/upload/media-manager";

export default function removeMediaManager(id) {
  return axiosClient.delete(
    `${process.env.SERVER_API_URL}${URL_API_DELETE_MEDIA_MANAGER}/${id}/delete`
  );
}

export function upload(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_DELETE_MEDIA_MANAGER}`,
    data
  );
}

export function patchUpload(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_DELETE_MEDIA_MANAGER}`,
    data
  );
}
