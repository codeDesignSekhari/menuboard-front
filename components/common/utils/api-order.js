import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_ORDER = "/order";
const URL_API_SUBSCRIPTION_PAYMENT = "/subscription";

export default function postBackOrder(data) {
  return axiosClient.post(`${process.env.SERVER_API_URL}/orders`, data);
}

export function patchBackOrder(data) {
  return axiosClient.patch(
    `${process.env.SERVER_API_URL}${URL_API_ORDER}/${data.id}`,
    data
  );
}

export function generateSubscriptionPayment(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_SUBSCRIPTION_PAYMENT}`,
    data
  );
}

export function queryDeleteOrder(id) {
  return axiosClient.delete(`${process.env.SERVER_API_URL}/orders/${id}`);
}
