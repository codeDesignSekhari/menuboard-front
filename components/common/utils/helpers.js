import {
  BACK,
  ROLE_ADMIN,
  ROLE_BACK,
} from "@/components/common/utils/const-roles";
import { HOME_FR } from "@/router/routeName";

const TYPE_OBJECT = "object";

export function generateStringRandom(length = 6) {
  let result = "";
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  const charactersLength = characters.length;
  let counter = 0;

  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }

  return result;
}

export function transformSplitString(stringToSplit, separator) {
  const lowerString = stringToSplit.toString().toLowerCase().trim();
  const arrayOfStrings = lowerString.split(separator);
  return arrayOfStrings.join("-");
}

export function transformPrice(number, currency = "€") {
  const price = parseFloat(number).toFixed(2).trim();
  const formattedPrice = price.toString().replace(".", ",");
  return `${formattedPrice} ${currency}`;
}

export function isEmpty(data) {
  const type = typeof data;

  let count = 0;
  if (type === TYPE_OBJECT) {
    for (const value of Object.entries(data)) {
      if (value === null || value === "" || value === undefined) {
        count++;
      }
    }
    return count;
  } else {
    return data === null || data === "" || data === undefined;
  }
}

export function copyData(data) {
  return Object.assign({}, data);
}

export function sortByRanK(data) {
  return data.sort((a, b) => a.rank - b.rank);
}

export function sortByPrice(data) {
  return data.sort((a, b) => a.price - b.price);
}

export function generatePrintPrice(item, percentage) {
  const myPrice = 40;
  const costPrice = (item * myPrice) / 100;
  let price = item + costPrice;
  if (percentage) {
    const pricePromote = (price * percentage) / 100;
    price = price - pricePromote;
  }

  return price;
}

export function getDateFromHours(time) {
  time = time.split(":");
  const now = new Date();
  return new Date(now.getFullYear(), now.getMonth(), now.getDate(), ...time);
}

export function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    const r = (Math.random() * 16) | 0;
    const v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export function getMenuByRole(items, user) {
  return items.filter((item) => {
    if (item.title === HOME_FR) {
      return true;
    }

    if (user) {
      const userRoles = user.roles;
      const access = item.access;

      if (
        (access === ROLE_ADMIN && [...userRoles].includes(ROLE_ADMIN)) ||
        (access === ROLE_BACK && [...userRoles].includes(ROLE_BACK))
      ) {
        return true;
      }
    }

    return false;
  });
}

export default function formatPhoneNumber(phone) {
  const cleaned = ("" + phone).replace(/\D/g, "");
  const match = cleaned.match(/(\d{0,2})(\d{0,2})(\d{0,2})(\d{0,2})(\d{0,2})/);
  if (match) {
    return (
      match[1] +
      " " +
      match[2] +
      " " +
      match[3] +
      " " +
      match[4] +
      " " +
      match[5]
    );
  }

  return null;
}

export function hasBackInUrl(config) {
  const url = config.url.replace(config.baseURL, "");

  const splitUrl = url.split("/");

  return splitUrl.includes(BACK);
}
