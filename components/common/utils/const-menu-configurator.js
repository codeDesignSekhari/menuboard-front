const childMenuConfigurator = {
  id: undefined,
  "@id": undefined,
  mediaManager: {
    id: undefined,
    name: undefined,
    fileName: undefined,
    size: undefined,
    assetId: undefined,
  },
  priceMenu: undefined,
  name: undefined,
  price: undefined,
  allergen: undefined,
  subTitle: undefined,
  description: undefined,
  supplementPrice: undefined,
  supplement: undefined,
  menuTitle: {
    id: undefined,
    name: undefined,
  },
  complementGroups: [],
};

export default childMenuConfigurator;
