export const ROLE_ADMIN = "ROLE_ADMIN";
export const ROLE_BACK = "ROLE_BACK";
export const BACK = "back";
export const ADMIN = "admin";


export default {
  ROLE_ADMIN,
  ROLE_BACK,
  BACK,
  ADMIN,
}
