import axiosClient from "@/components/common/utils/axiosClient";

export default function postCart(data) {
  return axiosClient.post(`${process.env.SERVER_API_URL}/carts`, data);
}
