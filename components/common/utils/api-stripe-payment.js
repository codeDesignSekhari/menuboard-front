import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_STRIPE_PAYMENT = "/payment";
const URL_API_SUBSCRIPTION_PAYMENT = "/subscription";

export default function generatePaymentProduct(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_STRIPE_PAYMENT}`,
    data
  );
}

export function generateSubscriptionPayment(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_SUBSCRIPTION_PAYMENT}`,
    data
  );
}
