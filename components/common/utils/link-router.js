import {
  BANNER_IMAGE,
  HOME_FR,
  ListUrls,
  MY_ACCOUNT_FR,
  MY_ORDER,
  ORDER_PRINT,
  PROVIDER,
  // ROOM_SERVICE_INTERFACE_TITLE,
  // ROOM_SERVICE_TITLE,
  RouteName,
} from "@/router/routeName";
import { ROLE_BACK, ROLE_ADMIN } from "~/components/common/utils/const-roles";

const URL_LINK_MAPS = [
  {
    title: HOME_FR,
    icon: "home-outline",
    link: ListUrls.BACKOFFICE,
  },
  {
    title: RouteName.CREATE_MENU,
    icon: "silverware-fork-knife",
    link: ListUrls.MENU_CONFIGURATOR,
    access: ROLE_BACK,
  },
  {
    title: MY_ORDER,
    icon: "basket-outline",
    link: ListUrls.ORDER,
    access: ROLE_BACK,
  },

  /** {
    title: ROOM_SERVICE_INTERFACE_TITLE,
    icon: "table-chair",
    link: ListUrls.ROOM_SERVICE_INTERFACE,
    access: ROLE_BACK,
  },**/

  {
    title: BANNER_IMAGE,
    icon: "image-frame",
    link: ListUrls.BANNER,
    access: ROLE_BACK,
  },

  {
    title: MY_ACCOUNT_FR,
    icon: "store",
    link: ListUrls.MY_ACCOUNT,
    access: ROLE_BACK,
  },

  {
    title: PROVIDER,
    icon: "factory",
    link: ListUrls.PROVIDER,
    access: ROLE_BACK,
  },

  {
    title: ORDER_PRINT,
    icon: "invert-colors",
    link: ListUrls.ORDER_PRINT,
    access: ROLE_BACK,
  },

  // {
  //   title: RouteName.ORDER_FR,
  //   icon: "cart-outline",
  //   link: "",
  //   access: ROLE_BACK,
  // },

  // {
  //   title: "Mes paramètres",
  //   icon: "cog-outline",
  //   link: "",
  //   access: ROLE_BACK,
  // },
  /** {
    title: ROOM_SERVICE_TITLE,
    icon: "room-service-outline",
    link: ListUrls.ROOM_SERVICE,
    access: ROLE_BACK,
  },**/

  // ADMIN
  {
    title: RouteName.REGISTER,
    icon: "account-plus",
    link: ListUrls.REGISTER,
    access: ROLE_ADMIN,
  },

  {
    title: RouteName.LIST_USERS,
    icon: "account-group-outline",
    link: ListUrls.LIST_USERS,
    access: ROLE_ADMIN,
  },
];

export default URL_LINK_MAPS;
