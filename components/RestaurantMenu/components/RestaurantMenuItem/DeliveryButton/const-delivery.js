export const DELIVERY = 2;
export const TAKE = 1;
export const ON_THE_SPOT = 0;

export const DELIVERY_COOKIE = "DELIVERY";
export const TAKE_COOKIE = "TAKE";
export const ON_THE_SPOT_COOKIE = "ON_THE_SPOT";

export const DELIVERY_FR = "Livraison";
export const TAKE_FR = "Sur place";
export const ON_THE_SPOT_FR = "Click and collect";

export const DELIVERY_MODE_MAP = {
  [DELIVERY_COOKIE]: DELIVERY,
  [TAKE_COOKIE]: TAKE,
  [ON_THE_SPOT_COOKIE]: ON_THE_SPOT,
};

export const MODE_DELIVERY_MAP = {
  [TAKE_COOKIE]: TAKE_FR,
  [DELIVERY_COOKIE]: DELIVERY_FR,
  [ON_THE_SPOT_COOKIE]: ON_THE_SPOT_FR,
};

export default MODE_DELIVERY_MAP;
