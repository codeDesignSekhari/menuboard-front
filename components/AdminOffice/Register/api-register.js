import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_POST_TERM_CONDITION = "/back/term-condition";

export default function registerUser(data) {
  return axiosClient.post(`/register`, data);
}

export function postAcceptTermCondition(data) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_TERM_CONDITION}`,
    data
  );
}
