import axiosClient from "@/components/common/utils/axiosClient";

const URL_API_GET_USER = "/back/user/";
const URL_API_POST_REQUEST_FORGET_PASSWORD = "/request/forgot_password";

export default function getUser(email) {
  return axiosClient.get(`${process.env.SERVER_API_URL}${URL_API_GET_USER}${email}`);
}

export function forgetPasswordRequest(email) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_REQUEST_FORGET_PASSWORD}`,
    email
  );
}

export function resetPassword(password) {
  return axiosClient.post(
    `${process.env.SERVER_API_URL}${URL_API_POST_REQUEST_FORGET_PASSWORD}?reset=true`,
    password
  );
}
