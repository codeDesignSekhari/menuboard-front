// import path from "path";
// import fs from "fs";
import colors from "vuetify/lib/util/colors";
require("dotenv").config();

const palette = {
  custom: {
    primary: colors.shades.black, // primary main
    primarylight: colors.grey.lighten2, // primary light
    primarydark: colors.grey.darken3, // primary dark
    secondary: colors.grey.darken3, // secondary main
    secondarylight: colors.grey.lighten2, // secondary light
    secondarydark: colors.lightGreen.darken3, // secondary dark
    anchor: colors.grey.lighten2, // link
  },
};

export const theme = {
  ...palette.custom,
};

export default {
  target: "static",
  ssr: false,
  // target: "server",
  env: {
    SERVER_API_URL: process.env.SERVER_API_URL,
    debug: process.env.NODE_ENV && process.env.NODE_ENV === "development",
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "Imenus - Digitalisation de vos menus",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Solution de commande et de réservation en ligne pour votre restaurant",
      },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ["~/assets/css/vuetify-overide.scss", "~/assets/css/globalFont.scss"],

  axios: {
    baseURL: process.env.SERVER_API_URL,
  },

  loading: {
    color: "black",
    height: "5px",
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "@/plugins/draggable", mode: "client" },
    // { src: "@/plugins/stripe", ssr: false },
    "@/plugins/axios",
    "@/plugins/auth",
    "@/plugins/vee-validate",
    { src: "@/plugins/vue-phone-number-input.js", ssr: false },
    "@/plugins/filters",
    // { src: "@/plugins/vuex-persist", ssr: false },
    "@/plugins/gtm",
    { src: "@/plugins/vue-fragment", ssr: false },
    { src: "@/plugins/moment", ssr: false },
    { src: "@/plugins/datePicker", ssr: false },
    { src: "@/plugins/chart", ssr: false },
    //{ src: "@/plugins/vueQrCode", ssr: false },
    { src: "@/plugins/vueTour", ssr: false }, // https://github.com/pulsardev/vue-tour
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    '@nuxt/typescript-build',
    // "nuxt-compress",
    // "@nuxtjs/pwa",
    "@nuxtjs/eslint-module",
    "@nuxtjs/vuetify",
    ["@nuxtjs/dotenv", { path: "./" }],
    "@nuxtjs/date-fns",
    "@nuxtjs/device",
    "@nuxtjs/google-analytics",
    // "@nuxtjs/html-validator",
    ["@nuxtjs/router", { path: "router", fileName: "router.js" }],
    // "nuxt-purgecss",
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Roboto: {
            wght: [400, 600, 700],
          },
        },
        subsets: ["latin"],
        display: "swap",
        prefetch: false,
        preconnect: true,
        preload: false,
        download: true,
        base64: false,
      },
    ],
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    "cookie-universal-nuxt",
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    "@nuxtjs/cloudinary",
    [
      "@nuxtjs/recaptcha",
      {
        siteKey: process.env.RECAPTCHA_SITE_KEY,
        version: 3,
      },
    ],
    "@nuxtjs/emotion",
    [
      "@nuxtjs/toast",
      {
        iconPack: "custom-class",
        theme: "toasted-primary",
        position: "top-right",
        duration: 5000,
        className: "toast-font",
        register: [
          {
            name: "success",
            message: (message) => message,
            options: {
              type: "success",
              icon: "mdi-check-outline",
            },
          },
          {
            name: "error",
            message: (message) => message,
            options: {
              type: "error",
              icon: "mdi-alert-octagon-outline",
            },
          },
          {
            name: "warning",
            message: (message) => message,
            options: {
              type: "info",
              icon: "mdi-alert-octagon-outline",
            },
          },
        ],
      },
    ],
    "vue-scrollto/nuxt",
    "@nuxtjs/robots",
    "@nuxtjs/sitemap",
    "@luxdamore/nuxt-prune-html",
    "@nuxt/content",
    "@nuxtjs/gtm",
    "@nuxtjs/moment",
    "vue2-editor/nuxt",
    "nuxt-stripe-module",
  ],

  stripe: {
    publishableKey: process.env.STRIPE_PK,
    locale: "fr_FR",
  },

  moment: {
    locales: ["fr"],
    // defaultTimezone: "Europe/Paris",
  },

  auth: {
    localStorage: false,
    cookie: {
      prefix: "auth.",
      options: {
        secure: true,
        maxAge: 432000, // 5 days
      },
    },
    token: {
      global: false,
    },
    plugins: [{ src: "~/plugins/axios", ssr: true }, "~/plugins/auth.js"],
    redirect: {
      login: "/login",
      logout: "/login",
    },
    strategies: {
      local: {
        endpoints: {
          logout: false,
          login: {
            url: "login_check",
            method: "post",
            property: "token",
          },
          refresh: { url: "token/refresh", method: "post" },
          user: {
            url: "back/user_check",
            method: "get",
            property: "data.user",
          },
        },
      },
    },
  },

  cloudinary: {
    secure: true,
    cloudName: process.env.CLOUDNAME,
    apiKey: process.env.API_KEY, // only needed if you are using server-side upload
    apiSecret: process.env.API_SECRET, // only needed if you are using server-side upload
  },

  sitemap: {
    path: "/sitemap.xml", // L'emplacement de votre fichier sitemap.
    hostname: process.env.PUBLIC_URL_FRONT, // L'adresse de votre site, que vous pouvez placer comme ici dans une variable d'environnement.
    cacheTime: 1000 * 60 * 15, // La durée avant que le sitemap soit regénéré. Ici 15mn.
    gzip: true,
    generate: false, // Génère une version statique du sitemap quand activé. À utiliser avec nuxt generate.
    exclude: ["/back/**", "/admin/**", "/login"],
    routes: ["/tandoori-house", "kitchen-hana-feyzin"],
  },

  robots: {
    Disallow: ["/login", "/back", "/admin"],
    Sitemap: `${process.env.PUBLIC_URL_FRONT}/sitemap.xml`,
  },

  gtm: {
    // Always send real GTM events (also when using `nuxt dev`)
    enabled: false,
  },

  pwa: {
    manifest: {
      name: "Imenus",
      lang: "fr",
      short_name: "Imenus App",
      theme_color: "#030303",
      background_color: "#FFF",
      publicPath: "/_nuxt/",
    },
    icon: {
      fileName: "imenus-logo.png",
      sizes: [64, 120, 144, 152, 192, 384, 512],
    },
    workbox: {
      publicPath: "/_nuxt/",
      config: {
        debug: true,
      },

      runtimeCaching: [
        {
          urlPattern: "https://fonts.googleapis.com/.*",
          handler: "cacheFirst",
          method: "GET",
        },

        {
          urlPattern: "https://.imenus.fr/.*",
          handler: "cacheFirst",
          strategyOptions: {
            cacheName: "imenus-front",
            cacheExpiration: {
              maxEntries: 100,
              maxAgeSeconds: 3600,
            },
          },
        },

        {
          urlPattern: "/images/.*",
          handler: "cacheFirst",
          strategyOptions: {
            cacheName: "images",
            cacheExpiration: {
              maxEntries: 20,
              maxAgeSeconds: 3600,
            },
          },
        },

        {
          urlPattern: "/v1/api/.*",
          handler: "networkOnly",
        },
      ],
      // cachingExtensions: "@/plugins/workbox-sync.js",
      enabled: true, // should be off actually per workbox docs due to complications when used in prod
    },
  },

  // html Validator #https://html-validator.nuxtjs.org/
  htmlValidator: {
    usePrettier: true,
    failOnError: false,
    options: {
      extends: [
        "html-validate:document",
        "html-validate:recommended",
        "html-validate:standard",
      ],
      rules: {
        "svg-focusable": "off",
        "no-unknown-elements": "error",
        // Conflicts or not needed as we use prettier formatting
        "void-style": "off",
        "no-console": "off",
        "no-trailing-whitespace": "off",
        // Conflict with Nuxt defaults
        "require-sri": "off",
        "attribute-boolean-style": "off",
        "doctype-style": "off",
        // Unreasonable rule
        "no-inline-style": "off",
      },
    },
  },

  googleAnalytics: {
    checkDuplicatedScript: true,
    id: process.env.GOOGLE_ANALYTICS_ID, // Use as fallback if no runtime config is provided
    autoTracking: {
      screenview: true,
    },
  },
  publicRuntimeConfig: {
    PUBLIC_BASE_URI: process.env.SERVER_API_URL,
    PUBLIC_URL_FRONT: process.env.PUBLIC_URL_FRONT,
    PUBLIC_URL_CLOUDINARY: process.env.PUBLIC_URL_CLOUDINARY,
    VERSION: process.env.VERSION,
  },
  privateRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID,
    },
    recaptcha: {
      siteKey: process.env.RECAPTCHA_SITE_KEY,
    },
  },

  render: {
    bundleRenderer: {
      directives: {
        shouldPreload: (file, type) => {
          return ["script", "style", "font"].includes(type);
        },
      },
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ["~/assets/css/styles.scss"],
    treeShake: true,
    theme: {
      themes: {
        light: {
          ...theme,
        },
        dark: {
          ...theme,
        },
      },
      disable: false,
      options: { customProperties: true },
    },
  },

  pruneHtml: {
    enabled: true, // `true` in production
    hideGenericMessagesInConsole: false, // `false` in production
    hideErrorsInConsole: false, // deactivate the `console.error` method
    hookRenderRoute: true, // activate `hook:render:route`
    hookGeneratePage: true, // activate `hook:generate:page`
    selectors: [
      // CSS selectors to prune
      'link[rel="preload"][as="script"]',
      'script:not([type="application/ld+json"])',
    ],
    classesSelectorsToKeep: [], // disallow pruning of scripts with this classes, n.b.: each `classesSelectorsToKeep` is appended to every `selectors`, ex.: `link[rel="preload"][as="script"]:not(__classesSelectorsToKeep__)`
    link: [], // inject custom links, only if pruned
    script: [], // inject custom scripts, only if pruned
    htmlElementClass: null, // a string added as a class to the <html> element if pruned
    cheerio: {
      // the config passed to the `cheerio.load(__config__)` method
      xmlMode: false,
    },
    types: [
      // it's possibile to add different rules for pruning
      "query-parameters",
      "default-detect",
      "headers-exist",
    ],

    // 👇🏻 Type: `default-detect`
    headerNameForDefaultDetection: "user-agent", // The `header-key` base for `MobileDetection`, usage `request.headers[ headerNameForDefaultDetection ]`
    auditUserAgent: "lighthouse", // prune if `request.header[ headerNameForDefaultDetection ]` match, could be a string or an array of strings
    isAudit: true, // remove selectors if match with `auditUserAgent`
    isBot: true, // remove selectors if is a bot
    ignoreBotOrAudit: false, // remove selectors in any case, not depending on Bot or Audit
    matchUserAgent: null, // prune if `request.header[ headerNameForDefaultDetection ]` match, could be a string or an array of strings

    // 👇🏻 Type: 'query-parameters'
    queryParametersToPrune: [
      // array of objects (key-value)
      // trigger the pruning if 'query-parameters' is present in `types` and at least one value is matched, ex. `/?prune=true`
      {
        key: "prune",
        value: "true",
      },
    ],
    queryParametersToExcludePrune: [
      {
        key: "prune",
        value: "false",
      },
    ], // same as `queryParametersToPrune`, exclude the pruning if 'query-parameters' is present in `types` and at least one value is matched, this priority is over than `queryParametersToPrune`

    // 👇🏻 Type: 'headers-exist'
    headersToPrune: [], // same as `queryParametersToPrune`, but it checks `request.headers`
    headersToExcludePrune: [{ key: "x-prune-html", value: "false" }], // same as `queryParamToExcludePrune`, but it checks `request.headers`, this priority is over than `headersToPrune`

    // Emitted events for callbacks methods
    onBeforePrune: null, // ({ result, [ req, res ] }) => {}, `req` and `res` are not available on `nuxt generate`
    onAfterPrune: null, // ({ result, [ req, res ] }) => {}, `req` and `res` are not available on `nuxt generate`
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    publicPath: "/_nuxt/",
    // analyze: true,
    babel: {
      compact: true,
      plugins: [
        "@babel/plugin-proposal-optional-chaining",
        "@babel/plugin-proposal-nullish-coalescing-operator",
      ],
    },
    optimization: {
      // minimize: true,
      runtimeChunk: true,
    },
    splitChunks: {
      layouts: false,
      pages: true,
      commons: true,
    },
    extractCSS: process.env.NODE_ENV === "production",
    html: {
      minify: {
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        removeComments: true,
        useShortDoctype: true,
        preserveLineBreaks: false,
        collapseWhitespace: false,
      },
    },
    transpile: [
      "vee-validate/dist/rules",
      "/plugins",
      "@googlemaps/js-api-loader",
    ],
    extend(config) {
      config.performance.hints = false;
      config.performance.maxAssetSize = 700 * 1024;

      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      });
    },
  },
};
