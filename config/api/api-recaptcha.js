import axiosClient from "@/components/common/utils/axiosClient";
import { verifyRecaptchaRoute } from "@/config/api/routes/verify-recaptcha-route-server";

export async function verifyRecaptcha(token) {
  return await axiosClient.post(verifyRecaptchaRoute, { token });
}
