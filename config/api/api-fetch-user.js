import axiosClient from "../../components/common/utils/axiosClient";
import { backUserCheckRoute } from "./routes/user-route";

export async function fetchUserCheck(store) {
  return await axiosClient
    .get(backUserCheckRoute())
    .then((response) => {
      const data = response?.data;

      if (data) {
        store.commit("fetchDefaultData/SET_RESTAURANT", data.user.restaurant);
        store.commit("fetchUser/SET_TO_USER", data.user.user);
      }

      return data;
    })
    .catch((e) => {
      console.error(e);
    });
}
