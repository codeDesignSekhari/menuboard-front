import {
  adminSeoUserRoute,
  seoUserIdRoute,
  seoUserRoute,
} from "@/config/api/routes/seo-route";
import axiosClient from "@/components/common/utils/axiosClient";

export async function fetchSeoAdminUser(store, params, baseUri) {
  return await axiosClient
    .get(adminSeoUserRoute(params.id, baseUri))
    .then(({ data }) => {
      if (data?.data) {
        store.commit("seo/SET_SEO", data.data.seo);
        store.commit("fetchUser/SET_USER_ID", data.data.user);
      }
    })
    .catch((e) => {
      console.error(e.response.data);
    });
}

export async function fetchSeoUser(store, route, baseUri) {
  if (route?.name) {
    return await axiosClient
      .get(seoUserRoute(route.name, baseUri))
      .then(({ data }) => {
        if (data?.data) {
          store.commit("seo/SET_SEO_FRONT", data.data);
        }
      })
      .catch((e) => {
        console.error(e.response.data);
      });
  }
}

export async function fetchSeoUserId(store, id) {
  try {
    return await axiosClient.get(seoUserIdRoute(id)).then((res) => {
      if (res?.data?.data) {
        store.commit("seo/SET_SEO_FRONT", res.data.data);
      }
    });
  } catch (e) {
    console.error(e);
  }
}
