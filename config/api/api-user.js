import axiosClient from "@/components/common/utils/axiosClient";
import { backUserRoute } from "@/config/api/routes/user-route";

export async function fetchBackUser(store, user, baseUri) {
  try {
    return await axiosClient
      .get(backUserRoute(user, baseUri))
      .then(({ data }) => {
        const response = data.data;
        store.commit(
          "menuConfigurator/SET_TO_MENU_CONFIGURATOR",
          response.menuConfigurators ?? []
        );

        store.commit("fetchDefaultData/SET_RESTAURANT", response.restaurant);
        store.commit("fetchUser/SET_TO_USER", response.user);
        store.commit("fetchUser/SET_TO_CATEGORIES", response.categories);

        return response;
      });
  } catch (e) {
    console.error(e);
  }
}
