import axiosClient from "../../components/common/utils/axiosClient";
import { refreshTokenRoute } from "./routes/user-route";

export async function apiRefreshToken(store, cookies) {
  console.log("refresh token");
  await axiosClient
    .post(refreshTokenRoute(), {
      data: { refresh_token: cookies.get("auth._refresh_token") },
    })
    .then(async (response) => {
      console.log("refresh response", response);
      cookies.set("auth._token", response.data.token);
      cookies.set("auth._refresh_token", response.data.refresh_token);
      await store.commit("fetchUser/SET_IS_LOGGED", true);
    })
    .catch((e) => {
      console.error(e);
    });
}
