import axiosClient from "../../components/common/utils/axiosClient";
import { loginCheckRoute } from "./routes/user-route";

export async function apiLoginCheck(store, cookies, data) {
  return await axiosClient
    .post(loginCheckRoute(), data)
    .then(async (response) => {
      await store.commit("auth-token/SET_TOKEN", response.data.token);
      await store.commit(
        "auth-token/SET_REFRESH_TOKEN",
        response.data.refresh_token
      );

      cookies.set("auth._token", response.data.token);
      cookies.set("auth._refresh_token", response.data.refresh_token);
      await store.commit("auth-token/SET_IS_LOGGED_IN", true);
    })
    .catch((e) => {
      console.error(e);
    });
}
