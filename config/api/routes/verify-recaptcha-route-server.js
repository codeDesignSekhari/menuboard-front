const BASE_URL = process.env.SERVER_API_URL;

export const verifyRecaptchaRoute = `${BASE_URL}/recaptcha-token`;
