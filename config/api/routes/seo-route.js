export function adminSeoUserRoute(id, baseUri) {
  return `${baseUri}/admin/seo/user/${id}`;
}

export function seoUserRoute(name, baseUri) {
  return `${baseUri}/seo/${name}`;
}

export function seoUserIdRoute(id) {
  return `/seo/user/${id}`;
}
