const BASE_URL = process.env.SERVER_API_URL;

export function backUserRoute(user) {
  return `${BASE_URL}/back/user/${user}`;
}

export function backUserCheckRoute() {
  return `${BASE_URL}/back/user_check`;
}

export function loginCheckRoute() {
  return `${BASE_URL}/login_check`;
}
export function refreshTokenRoute() {
  return `${BASE_URL}/token/refresh`;
}
