import axiosClient from "@/components/common/utils/axiosClient";
import { fetchSeoUserId } from "@/config/api/api-seo";

export async function fetchRestaurantData(
  store,
  params,
  error,
  redirect
) {
  try {
    if (params?.city && params?.category && params?.slug) {
      return redirect(`/${params.slug}`);
    }

    return await axiosClient
      .get(`/restaurants/${params.slug}`)
      .then(async ({ data }) => {
        const results = data;

        if (results) {
          if (results.menus.length) {
            store.commit(
              "menuConfigurator/SET_TO_MENU_CONFIGURATOR",
              results.menus
            );
          }

          store.commit("fetchDefaultData/SET_RESTAURANT", results.store);

          return await fetchSeoUserId(store, results.store.user.id);
        }
      });
  } catch (e) {
    console.error(e);
    if (e.response.status === 404) {
      error({ statusCode: 404, message: "Page not found" });
    }
  }
}
